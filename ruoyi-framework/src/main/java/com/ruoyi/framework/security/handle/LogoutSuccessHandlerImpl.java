package com.ruoyi.framework.security.handle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.outpatient.core.LoginPatient;
import com.ruoyi.outpatient.core.RedisBloomFilter;
import com.ruoyi.outpatient.mapper.IPersonLoginMapper;
import com.ruoyi.outpatient.service.patient.impl.PatientTokenService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import com.alibaba.fastjson.JSON;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.manager.AsyncManager;
import com.ruoyi.framework.manager.factory.AsyncFactory;
import com.ruoyi.framework.web.service.TokenService;

/**
 * 自定义退出处理类 返回成功
 * 
 * @author ruoyi
 */
@Configuration
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler
{
    @Autowired
    private TokenService tokenService;


    @Autowired
    private RedisCache redisCache;

    @Autowired
    private PatientTokenService patientTokenService;
    @Autowired
    private RedisBloomFilter bloomFilter;



    @Autowired
    private IPersonLoginMapper personMapper;

    /**
     * 退出处理
     * 
     * @return
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException
    {

        String person = request.getHeader("person");
        if (person.equals("person")){
            LoginPatient loginPatient = patientTokenService.getLoginPatient(request);
            if (StringUtils.isNotNull(loginPatient))
            {
                String userName = loginPatient.getUsername();
                // 删除用户缓存记录
                tokenService.delLoginUser(loginPatient.getToken());
                // 记录用户退出日志
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(userName, Constants.LOGOUT, "退出成功"));
            }
            ServletUtils.renderString(response, JSON.toJSONString(AjaxResult.error(HttpStatus.SUCCESS, "退出成功")));

//            1.当过滤器链将用户退出后,将用户所浏览的blog删除,并添加到布隆过滤器中
//                1.1 获取用户在上线过程中所浏览的blogId
            Set<Long> cacheSet = redisCache.getCacheSet("temporary:del:" + loginPatient.getPerson().getPersonId());

//            1.2 将set中用户观看的blog添加到布隆过滤器中
            // TODO: 2022/5/13 多个set添加到过滤器
//            bloomFilter.insert();
//            redisCache.executePipelined("bloomFilter:recommend",cacheSet);

//            1.3 将set和数据库对应blogId删除
            redisCache.deleteObject("temporary:del:" + loginPatient.getPerson().getPersonId());
            Object[] objects = cacheSet.toArray();

            List<Long> list=new ArrayList<>();
            for (int i=0;i<objects.length;i++){
                long l = Long.valueOf(String.valueOf(objects[i]));
                list.add(l);
            }

//             TODO: 2022/5/13 将推介给用户删除,防止推介重复
//            int i=personMapper.deletePersonAndBlogByList(list);



            return; //直接返回
        }

        LoginUser loginUser = tokenService.getLoginUser(request);
        if (StringUtils.isNotNull(loginUser))
        {
            String userName = loginUser.getUsername();
            // 删除用户缓存记录
            tokenService.delLoginUser(loginUser.getToken());
            // 记录用户退出日志
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(userName, Constants.LOGOUT, "退出成功"));
        }
        ServletUtils.renderString(response, JSON.toJSONString(AjaxResult.error(HttpStatus.SUCCESS, "退出成功")));
    }
}
