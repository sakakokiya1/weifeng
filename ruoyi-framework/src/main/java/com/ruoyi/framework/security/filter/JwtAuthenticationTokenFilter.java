package com.ruoyi.framework.security.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.outpatient.core.LoginPatient;
import com.ruoyi.outpatient.service.patient.impl.PatientTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.service.TokenService;
import oshi.driver.mac.net.NetStat;

/**
 * token过滤器 验证token有效性
 * 
 * @author ruoyi
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter
{



    @Autowired
    private PatientTokenService patientTokenService;

    @Autowired
    private TokenService tokenService;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {


        String s=request.getHeader("person");
        if (s==null) {

            //        先通过token获取springsecurity里的对象
            LoginUser loginUser = tokenService.getLoginUser(request);
//        如果对象不为空 且 认证了
            if (StringUtils.isNotNull(loginUser) && StringUtils.isNull(SecurityUtils.getAuthentication())) {
                tokenService.verifyToken(loginUser);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
            chain.doFilter(request, response);
        }

        else {
//            StringUtils.isNotNull(request.getHeader("person");
            LoginPatient loginPatient = patientTokenService.getLoginPatient(request);
//            对象信息不为空且上下文认证为空
            if (StringUtils.isNotNull(loginPatient) && StringUtils.isNull(SecurityUtils.getAuthentication())) {
//                刷新token时间
                patientTokenService.verifyToken(loginPatient);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginPatient, null, loginPatient.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

//                为上下文设置认证
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }

//        没有则过滤
            chain.doFilter(request, response);
        }
    }

}
