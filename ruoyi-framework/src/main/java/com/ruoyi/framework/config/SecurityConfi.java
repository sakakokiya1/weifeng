//package com.ruoyi.framework.config;
//
//
//
//import com.ruoyi.framework.security.filter.PatientJwtAuthenticationTokenFilter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//import org.springframework.security.web.authentication.logout.LogoutFilter;
//import org.springframework.web.filter.CorsFilter;
//import com.ruoyi.framework.security.filter.JwtAuthenticationTokenFilter;
//import com.ruoyi.framework.security.handle.AuthenticationEntryPointImpl;
//import com.ruoyi.framework.security.handle.LogoutSuccessHandlerImpl;
//
///**
// * spring security配置
// *
// * @author ruoyi
// */
//
///**  网址：https://www.jianshu.com/p/77b4835b6e8e
// *其中注解@EnableGlobalMethodSecurity有几个方法：
// *
// * prePostEnabled： 确定 前置注解[@PreAuthorize,@PostAuthorize,..] 是否启用
// * securedEnabled： 确定安全注解 [@Secured] 是否启用
// * jsr250Enabled： 确定 JSR-250注解 [@RolesAllowed..]是否启用 */
//
//@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
//@Configuration
//public class SecurityConfi
//{
//
//
//
//    @Configuration
//    @Order(1)
//    static class DefaultWebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//        @Autowired
//        @Qualifier("PatientDetailsServiceImpl")
//        private UserDetailsService PatientDetailsServiceImpl;
//
//
//        /**
//         * 认证失败处理类
//         */
//        @Autowired
//        private AuthenticationEntryPointImpl unauthorizedHandler;
//
//        /**
//         * 退出处理类
//         */
//        @Autowired
//        private LogoutSuccessHandlerImpl logoutSuccessHandler;
//
//
//        /**
//         * LoginPateint的过滤器  自己定义
//         */
//        @Autowired
//        private PatientJwtAuthenticationTokenFilter patientJwtAuthenticationTokenFilter;
//
//        /**
//         * 跨域过滤器
//         */
//        @Autowired
//        private CorsFilter corsFilter;
//
//        /**
//         * 解决 无法直接注入 AuthenticationManager
//         *
//         * @return
//         * @throws Exception
//         */
//        @Bean  //工厂本地的bean，让他在工厂中暴露出来
//        @Override
//        public AuthenticationManager authenticationManagerBean() throws Exception
//        {
//            return super.authenticationManagerBean();
//        }
//
//
//
//        @Override
//        protected void configure(HttpSecurity httpSecurity) throws Exception {
//
//
//            httpSecurity
//                    // CSRF禁用，因为不使用session
//                    .csrf().disable()
//                    // 认证失败处理类
//                    .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
//                    // 基于token，所以不需要session
//                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//                    // 过滤请求
//                    .antMatcher("/outpatient/patient/**")
//                    .authorizeRequests()//开启认证管理
//                    // 对于登录login 注册register 验证码captchaImage 允许匿名访问
//                    .antMatchers("/login", "/register", "/captchaImage").anonymous()
//                    .antMatchers(
//                            HttpMethod.GET,
//                            "/",
//                            "/*.html",
//                            "/**/*.html",
//                            "/**/*.css",
//                            "/**/*.js",
//                            "/profile/**"
//                    ).permitAll()
//                    .antMatchers("/swagger-ui.html").anonymous()
//                    .antMatchers("/swagger-resources/**").anonymous()
//                    .antMatchers("/webjars/**").anonymous()
//                    .antMatchers("/*/api-docs").anonymous()
//                    .antMatchers("/druid/**").anonymous()
//                    .antMatchers("/outpatient/patient/**").permitAll()
//                    // 除上面外的所有请求全部需要鉴权认证
//                    .anyRequest().authenticated()
//                    .and()
//                    .headers().frameOptions().disable();
//
//            httpSecurity.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);
//            // 添加JWT filter
//
////            UsernamePasswordAuthenticationFilter表单过滤之前使用自定义的这个patientJwtAuthenticationTokenFilter过滤器
//            httpSecurity.addFilterBefore(patientJwtAuthenticationTokenFilter,UsernamePasswordAuthenticationFilter.class);
//            // 添加CORS filter
//            httpSecurity.addFilterBefore(corsFilter, PatientJwtAuthenticationTokenFilter.class);
//            httpSecurity.addFilterBefore(corsFilter, LogoutFilter.class);
//
//
//        }
//
//
//        /**
//         * 强散列哈希加密实现
//         */
//        @Bean
//        public BCryptPasswordEncoder bCryptPasswordEncoder()
//        {
//            return new BCryptPasswordEncoder();
//        }
//
//        /**
//         * 身份认证接口
//         */
//        @Override
//        protected void configure(AuthenticationManagerBuilder auth) throws Exception
//        { auth.userDetailsService(PatientDetailsServiceImpl).passwordEncoder(bCryptPasswordEncoder());
//
//        }
//
//    }
//
//    @Configuration
//    @Order(2)
//    static class DefaultWebSecurityConfig2 extends WebSecurityConfigurerAdapter {
//
//
//        @Autowired
//        @Qualifier("UserDetailsServiceImpl")
//        private UserDetailsService UserDetailsServiceImpl;
//
//
//        /**
//         * 认证失败处理类
//         */
//        @Autowired
//        private AuthenticationEntryPointImpl unauthorizedHandler;
//
//        /**
//         * 退出处理类
//         */
//        @Autowired
//        private LogoutSuccessHandlerImpl logoutSuccessHandler;
//
//        /**
//         * LoginUser的过滤器   token认证过滤器
//         */
//        @Autowired
//        private JwtAuthenticationTokenFilter authenticationTokenFilter;
//
//        /**
//         * 跨域过滤器
//         */
//        @Autowired
//        private CorsFilter corsFilter;
//
//        /**
//         * 解决 无法直接注入 AuthenticationManager
//         *
//         * @return
//         * @throws Exception
//         */
//        @Bean
//        @Override
//        public AuthenticationManager authenticationManagerBean() throws Exception
//        {
//            return super.authenticationManagerBean();
//        }
//
//        @Override
//        protected void configure(HttpSecurity httpSecurity) throws Exception {
//
//
//            httpSecurity
//                    // CSRF禁用，因为不使用session
//                    .csrf().disable()
//                    // 认证失败处理类
//                    .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
//                    // 基于token，所以不需要session
//                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//                    // 过滤请求
//                    .authorizeRequests()//开启认证管理
//                    // 对于登录login 注册register 验证码captchaImage 允许匿名访问
//                    .antMatchers("/login", "/register", "/captchaImage").anonymous()
//                    .antMatchers(
//                            HttpMethod.GET,
//                            "/",
//                            "/*.html",
//                            "/**/*.html",
//                            "/**/*.css",
//                            "/**/*.js",
//                            "/profile/**"
//                    ).permitAll()
//                    .antMatchers("/swagger-ui.html").anonymous()
//                    .antMatchers("/swagger-resources/**").anonymous()
//                    .antMatchers("/webjars/**").anonymous()
//                    .antMatchers("/*/api-docs").anonymous()
//                    .antMatchers("/druid/**").anonymous()
////                    .antMatchers("/outpatient/patient/**").permitAll()
//                    // 除上面外的所有请求全部需要鉴权认证
//                    .anyRequest().authenticated()
//                    .and()
//                    .headers().frameOptions().disable();
//            httpSecurity.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);
//            // 添加JWT filter
//            httpSecurity.addFilterBefore(authenticationTokenFilter,UsernamePasswordAuthenticationFilter.class);
//            // 添加CORS filter
//            httpSecurity.addFilterBefore(corsFilter, JwtAuthenticationTokenFilter.class);
//            httpSecurity.addFilterBefore(corsFilter, LogoutFilter.class);
//
//
//        }
//
//        /**
//         * 强散列哈希加密实现
//         */
//        @Bean
//        public BCryptPasswordEncoder bCryptPasswordEncoder()
//        {
//            return new BCryptPasswordEncoder();
//        }
//
//        /**
//         * 身份认证接口
//         */
//        @Override
//        protected void configure(AuthenticationManagerBuilder auth) throws Exception
//        {
//            auth.userDetailsService(UserDetailsServiceImpl).passwordEncoder(bCryptPasswordEncoder());
//
//        }
//
//
//    }
//
//
//
//}
