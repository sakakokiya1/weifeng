package com.ruoyi.framework.web.service;

import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.outpatient.core.LoginPatient;
import com.ruoyi.outpatient.domain.PatientIdentity;
import com.ruoyi.outpatient.domain.Person;
import com.ruoyi.outpatient.mapper.IPersonLoginMapper;
import com.ruoyi.outpatient.mapper.patient.PatientRegMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.UserStatus;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysUserService;

/**
 * 用户验证处理
 *
 * @author ruoyi
 */
@Service
@Qualifier("UserDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService
{
    private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private PatientRegMapper regMapper;

    @Autowired
    private IPersonLoginMapper personLoginMapper;

    @Autowired
    private RedisCache redisCache;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {


//        1.先获取redis中的用户
        


//        1.将账户进行解析,可能是user或patient，后期设置不同过滤器链
//        user_张三或 patien_李四
        String[] s = username.split("_");

        if (s[0].equals("person") || s[0].equals("null")){

            //        查询到返回，他会自己进行认证


//            PatientIdentity identity=regMapper.selectPatientBymedicalCard(s[1]);
            Person person = personLoginMapper.selectPersion(s[1]);

            if (StringUtils.isNull(person))
            {
                throw new ServiceException("该用户不存在");
            }
            if (StringUtils.isNull(person.getPersonId()))
            {
                throw new ServiceException("请先用身份证认证");
            }


//        2.患者权限设置


//        3.先存储信息后期加权限
            return new LoginPatient(person);

        }else if (true){

            SysUser user = userService.selectUserByUserName(username);
            if (StringUtils.isNull(user))
            {
                log.info("登录用户：{} 不存在.", s[1]);
                throw new ServiceException("登录用户：" + s[1] + " 不存在");
            }
            else if (UserStatus.DELETED.getCode().equals(user.getDelFlag()))
            {
                log.info("登录用户：{} 已被删除.", s[1]);
                throw new ServiceException("对不起，您的账号：" + s[1] + " 已被删除");
            }
            else if (UserStatus.DISABLE.getCode().equals(user.getStatus()))
            {
                log.info("登录用户：{} 已被停用.", s[1]);
                throw new ServiceException("对不起，您的账号：" + s[1] + " 已停用");
            }

            return createLoginUser(user);
        }


        return null;

    }

    public UserDetails createLoginUser(SysUser user)
    {
        return new LoginUser(user.getUserId(), user.getDeptId(), user, permissionService.getMenuPermission(user));
    }

}
