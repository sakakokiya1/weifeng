package com.ruoyi.outpatient.domain;

import java.util.Date;

/**
 * @author 伟峰
 * @date 2022/5/6
 * @description: 签到实体类
 */
public class Sign {

    private Long signId;

    private Long person_id;

    private Date year;

    private Integer month;

    private Date date;

    private Integer retroactive;

    public Long getSignId() {
        return signId;
    }

    public void setSignId(Long signId) {
        this.signId = signId;
    }

    public Long getPerson_id() {
        return person_id;
    }

    public void setPerson_id(Long person_id) {
        this.person_id = person_id;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getRetroactive() {
        return retroactive;
    }

    public void setRetroactive(Integer retroactive) {
        this.retroactive = retroactive;
    }
}
