package com.ruoyi.outpatient.domain;

import java.util.Date;
import java.util.List;

/**
 * @author 伟峰
 * @date 2022/5/18
 * @description: 封装评论信息实体类
 */
public class CommentVo {

    /**
     * 用户
     */
    private Long personId;

    private String personName;

    private String avatar;


    /**
     * 评论
     */
    private Long commentId;

    private String commentContent;

    private Date createTime;

    //    这条评论的回复总数,默认0
    private Integer replySum;

    //    这条评论的点赞量
    private Integer commentLikes;


    private List<Reply> replyList;


    public List<Reply> getReplyList() {
        return replyList;
    }

    public void setReplyList(List<Reply> replyList) {
        this.replyList = replyList;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getReplySum() {
        return replySum;
    }

    public void setReplySum(Integer replySum) {
        this.replySum = replySum;
    }

    public Integer getCommentLikes() {
        return commentLikes;
    }

    public void setCommentLikes(Integer commentLikes) {
        this.commentLikes = commentLikes;
    }


    @Override
    public String toString() {
        return "CommentVo{" +
                "personId=" + personId +
                ", personName='" + personName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", commentId=" + commentId +
                ", commentContent='" + commentContent + '\'' +
                ", createTime=" + createTime +
                ", replySum=" + replySum +
                ", commentLikes=" + commentLikes +
                ", replyList=" + replyList +
                '}';
    }
}
