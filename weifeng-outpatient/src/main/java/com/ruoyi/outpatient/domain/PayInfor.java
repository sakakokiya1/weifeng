package com.ruoyi.outpatient.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 缴费实体类
 */
public class PayInfor extends BaseEntity {

//    缴费ID
    private Long payId;
//    挂号单号
    private String registerNumber;
//    缴费日期
    private Date payDate;
//    缴费状态(0未缴费,1已缴费)
    private String payStatus;
//    缴费类型
    private String payType;
//    缴费详情
    private String payDetaile;


    public Long getPayId() {
        return payId;
    }

    public void setPayId(Long payId) {
        this.payId = payId;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber == null ? null : registerNumber.trim();
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus == null ? null : payStatus.trim();
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    public String getPayDetaile() {
        return payDetaile;
    }

    public void setPayDetaile(String payDetaile) {
        this.payDetaile = payDetaile == null ? null : payDetaile.trim();
    }

    @Override
    public String toString() {
        return "PayInfor{" +
                "payId=" + payId +
                ", registerNumber='" + registerNumber + '\'' +
                ", payDate=" + payDate +
                ", payStatus='" + payStatus + '\'' +
                ", payType='" + payType + '\'' +
                ", payDetaile='" + payDetaile + '\'' +
                '}';
    }
}