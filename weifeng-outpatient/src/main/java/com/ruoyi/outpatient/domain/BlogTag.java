package com.ruoyi.outpatient.domain;

/**
 * @author 伟峰
 * @date 2022/4/22
 * @description: blog和tag的关联外键
 */


public class BlogTag {

    private Long id;

    private Long blogId;

    private Long tagId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }
}
