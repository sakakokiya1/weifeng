package com.ruoyi.outpatient.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 *  检查实体类
 */
public class PatientChecked extends BaseEntity {
//    检查ID
    private Long checkId;
//    患者名字
    private String patientName;
//    诊疗卡
    private Long medicalCard;
//    检查单号
    private String checkNumber;
//    检查日期
    private Date checkDate;
//    检查类型
    private String checkType;
//    检查结果
    private String checkResult;
//    检查状态(0未检测，1已检测)


    private Date updateTime;

    public Long getCheckId() {
        return checkId;
    }

    public void setCheckId(Long checkId) {
        this.checkId = checkId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName == null ? null : patientName.trim();
    }

    public Long getMedicalCard() {
        return medicalCard;
    }

    public void setMedicalCard(Long medicalCard) {
        this.medicalCard = medicalCard;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber == null ? null : checkNumber.trim();
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType == null ? null : checkType.trim();
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult == null ? null : checkResult.trim();
    }

    @Override
    public String toString() {
        return "PatientChecked{" +
                "checkId=" + checkId +
                ", patientName='" + patientName + '\'' +
                ", medicalCard=" + medicalCard +
                ", checkNumber='" + checkNumber + '\'' +
                ", checkDate=" + checkDate +
                ", checkType='" + checkType + '\'' +
                ", checkResult='" + checkResult + '\'' +
                ", updateTime=" + updateTime +
                '}';
    }
}