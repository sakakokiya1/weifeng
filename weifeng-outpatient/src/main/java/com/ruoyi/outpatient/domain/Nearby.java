package com.ruoyi.outpatient.domain;

/**
 * @author 伟峰
 * @date 2022/5/18
 * @description: 附近的人 实体类
 */
public class Nearby {

//    位置
    private String location;

//    经度
    private double x;

//    维度
    private double y;


//    距离
    private String distance;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
