package com.ruoyi.outpatient.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.NotBlank;
import java.util.Arrays;
import java.util.List;

/**
 * @author 伟峰
 * @date 2022/4/22
 * @description:  标签分类实体类
 */
public class Tag extends BaseEntity{

    private Long tagId;

    private String tagName;

//    标签名数组
    private String[] tagNameArray;

    private Long blogId;

    private Blog blog;

    private List<Blog> blogs;

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }


    @NotBlank(message = "标签名不能为空")
    public String[] getTagNameArray() {
        return tagNameArray;
    }

    public void setTagNameArray(String[] tagNameArray) {
        this.tagNameArray = tagNameArray;
    }


    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "tagId=" + tagId +
                ", tagName='" + tagName + '\'' +
                ", tagNameArray=" + Arrays.toString(tagNameArray) +
                ", blogId=" + blogId +
                ", blog=" + blog +
                ", blogs=" + blogs +
                '}';
    }
}
