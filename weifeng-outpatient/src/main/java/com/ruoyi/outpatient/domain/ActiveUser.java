package com.ruoyi.outpatient.domain;

/**
 * @author 伟峰
 * @date 2022/5/31
 * @description: 活跃用户
 */
public class ActiveUser {

    private Long active_Id;

    private Long personId;

    public Long getActive_Id() {
        return active_Id;
    }

    public void setActive_Id(Long active_Id) {
        this.active_Id = active_Id;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }
}
