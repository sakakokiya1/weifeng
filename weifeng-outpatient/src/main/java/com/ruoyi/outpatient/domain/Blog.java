package com.ruoyi.outpatient.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;


/**
 * 分享说说实体类
 *
 * @author weifeng
 * @date 2022/4/20
 */
public class Blog extends BaseEntity {
    private Long blogId;

//    图片URL
    private String blogImage;

    private Character IV;

    private String[] blogImageArr;
//    主题
    private String blogTheme;

//    说说
    private String blogTalk;

//    状态
    private String status;




//    关联外键用户ID
    private Long personId;

//    外键关联标签ID
    private Long tagId;

//    点赞数
    private Integer likes;
    

    private Tag tag;

    private List<Tag> tags;


    private Person person;


//    浏览量
    private Long pageView;


    private Nearby nearby;

    private Integer commentsSum;

    private Character videoOrImage;

    private Integer collectSum;


//    第一帧
    private String firstFrame;


    //    判断用户是否点赞
    private Boolean likesIs;
    private Boolean collectIs;


    public String getFirstFrame() {
        return firstFrame;
    }

    public void setFirstFrame(String firstFrame) {
        this.firstFrame = firstFrame;
    }

    public Character getIV() {
        return IV;
    }

    public void setIV(Character IV) {
        this.IV = IV;
    }

    public Integer getCommentsSum() {
        return commentsSum;
    }

    public void setCommentsSum(Integer commentsSum) {
        this.commentsSum = commentsSum;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Boolean getLikesIs() {
        return likesIs;
    }

    public void setLikesIs(Boolean likesIs) {
        this.likesIs = likesIs;
    }

    public Boolean getCollectIs() {
        return collectIs;
    }

    public void setCollectIs(Boolean collectIs) {
        this.collectIs = collectIs;
    }

    public Integer getCollectSum() {
        return collectSum;
    }

    public void setCollectSum(Integer collectSum) {
        this.collectSum = collectSum;
    }

    public Character getVideoOrImage() {
        return videoOrImage;
    }

    public void setVideoOrImage(Character videoOrImage) {
        this.videoOrImage = videoOrImage;
    }

    public String[] getBlogImageArr() {
        return blogImageArr;
    }

    public void setBlogImageArr(String[] blogImageArr) {
        this.blogImageArr = blogImageArr;
    }

    public Nearby getNearby() {
        return nearby;
    }

    public void setNearby(Nearby nearby) {
        this.nearby = nearby;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public String getBlogImage() {
        return blogImage;
    }

    public void setBlogImage(String blogImage) {
        this.blogImage = blogImage;
    }


//    @NotBlank(message = "主题内容不能为空")
//    @Size(max = 20,message = "主题内容过长")
    public String getBlogTheme() {
        return blogTheme;
    }

    public void setBlogTheme(String blogTheme) {
        this.blogTheme = blogTheme;
    }

    public String getBlogTalk() {
        return blogTalk;
    }

    public void setBlogTalk(String blogTalk) {
        this.blogTalk = blogTalk;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }




    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }


    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }








    public Long getPageView() {
        return pageView;
    }

    public void setPageView(Long pageView) {
        this.pageView = pageView;
    }


}