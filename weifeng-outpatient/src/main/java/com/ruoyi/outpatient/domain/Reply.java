package com.ruoyi.outpatient.domain;

import java.util.Date;



/**
 * 回复实体类
 *
 * @author 伟峰
 * @date 2022/4/31
 */
public class Reply {
//    回复id
    private Long replyId;

//    即该回复是在哪个评论下回复的
    private Long commentId;

//    对哪个进行回复的id,可能是评论,也可能是回复,如果reply_type是comment的话，那么reply_id＝commit_id，如果reply_type是reply的话，这表示这条回复的父回复。
    private Long replygoalId;

//    回复类型,可以对评论回复，可以对回复来回复（1:对评论，0对回复）
    private Character replyType;

//    回复的内容
    private String replyContent;

//    该回复的用户id
    private Long myId;
    private String personName;
    private String avatar;

//    对哪个人进行回复的用户id
    private Long hisId;

//    这条回复的点赞量
    private Integer replyLikes;

//    创建时间
    private Date createTime;


    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Long getReplyId() {
        return replyId;
    }

    public void setReplyId(Long replyId) {
        this.replyId = replyId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getReplygoalId() {
        return replygoalId;
    }

    public void setReplygoalId(Long replygoalId) {
        this.replygoalId = replygoalId;
    }

    public Character getReplyType() {
        return replyType;
    }

    public void setReplyType(Character replyType) {
        this.replyType = replyType;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public Long getMyId() {
        return myId;
    }

    public void setMyId(Long myId) {
        this.myId = myId;
    }

    public Long getHisId() {
        return hisId;
    }

    public void setHisId(Long hisId) {
        this.hisId = hisId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getReplyLikes() {
        return replyLikes;
    }

    public void setReplyLikes(Integer replyLikes) {
        this.replyLikes = replyLikes;
    }
}