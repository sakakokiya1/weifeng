package com.ruoyi.outpatient.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 药瓶实体类
 */
public class Pharmacy extends BaseEntity {
    //用药ID
    private Long pharmacyId;
//    诊疗卡
    private Long medicalCard;
//    患者名字
    private String patientName;
//    药品单号
    private String drugNumber;
//    开药日期
    private Date pharmacyDate;
//    中西药
    private String pharmacyCe;
//    处方药或其他药
    private String pharmacyType;
//    用药状态(0未支付 1已支付)
    private String pharmacyStatus;


    public Long getPharmacyId() {
        return pharmacyId;
    }

    public void setPharmacyId(Long pharmacyId) {
        this.pharmacyId = pharmacyId;
    }

    public Long getMedicalCard() {
        return medicalCard;
    }

    public void setMedicalCard(Long medicalCard) {
        this.medicalCard = medicalCard;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName == null ? null : patientName.trim();
    }

    public String getDrugNumber() {
        return drugNumber;
    }

    public void setDrugNumber(String drugNumber) {
        this.drugNumber = drugNumber == null ? null : drugNumber.trim();
    }

    public Date getPharmacyDate() {
        return pharmacyDate;
    }

    public void setPharmacyDate(Date pharmacyDate) {
        this.pharmacyDate = pharmacyDate;
    }

    public String getPharmacyCe() {
        return pharmacyCe;
    }

    public void setPharmacyCe(String pharmacyCe) {
        this.pharmacyCe = pharmacyCe == null ? null : pharmacyCe.trim();
    }

    public String getPharmacyType() {
        return pharmacyType;
    }

    public void setPharmacyType(String pharmacyType) {
        this.pharmacyType = pharmacyType == null ? null : pharmacyType.trim();
    }

    public String getPharmacyStatus() {
        return pharmacyStatus;
    }

    public void setPharmacyStatus(String pharmacyStatus) {
        this.pharmacyStatus = pharmacyStatus == null ? null : pharmacyStatus.trim();
    }

    @Override
    public String toString() {
        return "Pharmacy{" +
                "pharmacyId=" + pharmacyId +
                ", medicalCard=" + medicalCard +
                ", patientName='" + patientName + '\'' +
                ", drugNumber='" + drugNumber + '\'' +
                ", pharmacyDate=" + pharmacyDate +
                ", pharmacyCe='" + pharmacyCe + '\'' +
                ", pharmacyType='" + pharmacyType + '\'' +
                ", pharmacyStatus='" + pharmacyStatus + '\'' +
                '}';
    }
}