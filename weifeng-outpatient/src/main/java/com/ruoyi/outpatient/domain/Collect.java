package com.ruoyi.outpatient.domain;

import java.util.Date;

/**
 * 收藏实体类
 */
public class Collect {
    private Long collectId;

    private Long personId;

    private Long collectBlogId;

    private Date createTime;

    public Long getCollectId() {
        return collectId;
    }

    public void setCollectId(Long collectId) {
        this.collectId = collectId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getCollectBlogId() {
        return collectBlogId;
    }

    public void setCollectBlogId(Long collectBlogId) {
        this.collectBlogId = collectBlogId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}