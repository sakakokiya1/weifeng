package com.ruoyi.outpatient.domain;

/**
 * @author 伟峰
 * @date 2022/5/10
 * @description: 支付实体类
 */
public class PayEnpity {

    private String product_code; //产品号
    private String out_trade_no; //商户订单号 必填
    private String subject; //订单名称  必填
    private String total_amount; //付款金额 必填
    private String body; //商品描述 选填


    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "PayEnpity{" +
                "out_trade_no='" + out_trade_no + '\'' +
                ", subject='" + subject + '\'' +
                ", total_amount='" + total_amount + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
