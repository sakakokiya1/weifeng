package com.ruoyi.outpatient.domain;

/**
 * 推荐相似度类
 *
 * @author 郑伟峰
 * @date 2022/5/22
 */
public class Similarity {
    private Long similarityId;

    private Long personId;

    private Long blogId;

    private Double similar;


    public Similarity(Long personId, Long blogId, Double similar) {
        this.personId = personId;
        this.blogId = blogId;
        this.similar = similar;
    }

    public Similarity(Long similarityId, Long personId, Long blogId, Double similar) {
        this.similarityId = similarityId;
        this.personId = personId;
        this.blogId = blogId;
        this.similar = similar;
    }

    public Long getSimilarityId() {
        return similarityId;
    }

    public void setSimilarityId(Long similarityId) {
        this.similarityId = similarityId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public Double getSimilar() {
        return similar;
    }

    public void setSimilar(Double similar) {
        this.similar = similar;
    }

}