package com.ruoyi.outpatient.domain;

import java.util.Date;

/**
 * 病例实体类
 */
public class PatientCase {

    private Long caseId;

    private Long medicalCard;

    private String patientName;

    private Date caseTime;

    private String sickType;

    private String visitOne;

    private String checkInfor;

    private String infectSick;

    private String status;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

    public Long getCaseId() {
        return caseId;
    }

    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    public Long getMedicalCard() {
        return medicalCard;
    }

    public void setMedicalCard(Long medicalCard) {
        this.medicalCard = medicalCard;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName == null ? null : patientName.trim();
    }

    public Date getCaseTime() {
        return caseTime;
    }

    public void setCaseTime(Date caseTime) {
        this.caseTime = caseTime;
    }

    public String getSickType() {
        return sickType;
    }

    public void setSickType(String sickType) {
        this.sickType = sickType == null ? null : sickType.trim();
    }

    public String getVisitOne() {
        return visitOne;
    }

    public void setVisitOne(String visitOne) {
        this.visitOne = visitOne == null ? null : visitOne.trim();
    }

    public String getCheckInfor() {
        return checkInfor;
    }

    public void setCheckInfor(String checkInfor) {
        this.checkInfor = checkInfor == null ? null : checkInfor.trim();
    }

    public String getInfectSick() {
        return infectSick;
    }

    public void setInfectSick(String infectSick) {
        this.infectSick = infectSick == null ? null : infectSick.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }



    @Override
    public String toString() {
        return "PatientCase{" +
                "caseId=" + caseId +
                ", medicalCard=" + medicalCard +
                ", patientName='" + patientName + '\'' +
                ", caseTime=" + caseTime +
                ", sickType='" + sickType + '\'' +
                ", visitOne='" + visitOne + '\'' +
                ", checkInfor='" + checkInfor + '\'' +
                ", infectSick='" + infectSick + '\'' +
                ", status='" + status + '\'' +
                ", createBy='" + createBy + '\'' +
                ", createTime=" + createTime +
                ", updateBy='" + updateBy + '\'' +
                ", updateTime=" + updateTime +
                '}';
    }
}