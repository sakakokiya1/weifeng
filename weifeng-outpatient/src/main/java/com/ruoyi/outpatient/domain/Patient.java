package com.ruoyi.outpatient.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.outpatient.service.impl.OptRegistrantionServiceImpl;

import java.util.Date;

/**
 * 患者实体类
 */
public class Patient extends BaseEntity {
//    患者ID
    private Long patientId;

    private OptRegistrantionServiceImpl optRegistrantionService;
//    患者名字
    private String patientName;

//    身份证
    private Long patientSid;

//    性别
    private String sex;

//    电话
    private String phonenumber;

//    年龄
    private Integer age;

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName == null ? null : patientName.trim();
    }

    public Long getPatientSid() {
        return patientSid;
    }

    public void setPatientSid(Long patientSid) {
        this.patientSid = patientSid;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber == null ? null : phonenumber.trim();
    }


    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return "Patient{" +
                "patientId=" + patientId +
                ", patientName='" + patientName + '\'' +
                ", patientSid=" + patientSid +
                ", sex='" + sex + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                ", age=" + age +
                '}';
    }
}