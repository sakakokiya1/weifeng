package com.ruoyi.outpatient.domain;

import java.math.BigDecimal;

/**
 * @author 伟峰
 * @date 2022/5/11
 * @description: 订单信息
 */
public class Order {

//    订单号
    private String orderNumber;

//    用户名
    private String personName;

//    订单总额
    private BigDecimal totalAmount;

//    支付总额
    private BigDecimal payAmount;

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }
}
