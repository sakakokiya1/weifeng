package com.ruoyi.outpatient.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * 患者身份证信息实体类
 */
public class PatientIdentity  extends BaseEntity {

//    身份ID
    private Long identityId;
//    患者名字
    private String patientName;
//    患者身份证
    private String identityCard;
//    诊疗卡号
    private String medicalCard;
//    诊疗卡密码
    private String medicalPassword;


//    状态
    private Character status;



    public Long getIdentityId() {
        return identityId;
    }

    public void setIdentityId(Long identityId) {
        this.identityId = identityId;
    }



    @NotBlank(message = "名字不能为空")
    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName == null ? null : patientName.trim();
    }

    @NotBlank(message = "身份证号码不能为空")
//    @Pattern(regexp = "/(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)/",message = "身份证格式不正确")
//    @Size(max = 18,min = 15,message = "身份证格式不正确")
    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard == null ? null : identityCard.trim();
    }

    /**
     * 诊疗卡格式= 4为身份证最后4位+随机生成4位
     *
     */
    @NotBlank(message = "诊疗卡不能为空")
    @Size(max = 8,min = 8,message = "诊疗卡格式不正确")
    public String getMedicalCard() {
        return medicalCard;
    }

    public void setMedicalCard(String medicalCard) {
        this.medicalCard = medicalCard == null ? null : medicalCard.trim();
    }

    @NotBlank(message = "密码不能为空") //密码有8到15数字字母组成
//    @Pattern(regexp = "/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,15}$/",message = "密码格式不正确")
    public String getMedicalPassword() {
        return medicalPassword;
    }

    public void setMedicalPassword(String medicalPassword) {
        this.medicalPassword = medicalPassword == null ? null : medicalPassword.trim();
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PatientIdentity{" +
                "identityId=" + identityId +
                ", patientName='" + patientName + '\'' +
                ", identityCard='" + identityCard + '\'' +
                ", medicalCard='" + medicalCard + '\'' +
                ", medicalPassword='" + medicalPassword + '\'' +
                ", status=" + status +
                '}';
    }
}