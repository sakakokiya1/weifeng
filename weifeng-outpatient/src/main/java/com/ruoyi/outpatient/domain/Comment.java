package com.ruoyi.outpatient.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;


/**
 * 评论实体类
 *
 * @author 伟峰
 * @date 2022/4/31
 */
public class Comment {

    private Long commentId;

//    分享ID
    private Long blogId;

//    分享内容
    private String commentContent;



//    评论创建时间
    private Date createTime;

//    这条评论的回复总数,默认0
    private Integer replySum;

//    这条评论的点赞量
    private Integer commentLikes;


    //    该评论的用户id
    private Long personId;
    private String personName;
    private String avatar;

    private List<Reply> reply;



    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }


    @NotBlank(message = "分享ID不能为空")
    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    @NotBlank(message = "评论内容不能为空")
    @Size(max = 140,message = "超过评论长度")
    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public List<Reply> getReply() {
        return reply;
    }

    public void setReply(List<Reply> reply) {
        this.reply = reply;
    }

    public Integer getReplySum() {
        return replySum;
    }

    public void setReplySum(Integer replySum) {
        this.replySum = replySum;
    }

    public Integer getCommentLikes() {
        return commentLikes;
    }

    public void setCommentLikes(Integer commentLikes) {
        this.commentLikes = commentLikes;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentId=" + commentId +
                ", blogId=" + blogId +
                ", commentContent='" + commentContent + '\'' +
                ", createTime=" + createTime +
                ", replySum=" + replySum +
                ", commentLikes=" + commentLikes +
                ", personId=" + personId +
                ", personName='" + personName + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}