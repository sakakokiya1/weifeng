package com.ruoyi.outpatient.domain;

import cn.hutool.core.date.DateTime;

import java.io.Serializable;

/**
 * @author 伟峰
 * @date 2022/5/4
 * @description:
 */
public class Follow implements Serializable {

    private Long followId;

    private Long personId;

    private Long followPersonId;

    private DateTime createTime;


    public Long getFollowId() {
        return followId;
    }

    public void setFollowId(Long followId) {
        this.followId = followId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getFollowPersonId() {
        return followPersonId;
    }

    public void setFollowPersonId(Long followPersonId) {
        this.followPersonId = followPersonId;
    }

    public DateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(DateTime createTime) {
        this.createTime = createTime;
    }
}
