package com.ruoyi.outpatient.domain;

import java.util.Date;

public class Likes {
    private Long likesId;

    private Long personId;

    private String likesType;

    private Long likeTypeId;

    private Date createTime;

    private Character likesStatus;

    public Character getLikesStatus() {
        return likesStatus;
    }

    public void setLikesStatus(Character likesStatus) {
        this.likesStatus = likesStatus;
    }

    public Long getLikesId() {
        return likesId;
    }

    public void setLikesId(Long likesId) {
        this.likesId = likesId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getLikesType() {
        return likesType;
    }

    public void setLikesType(String likesType) {
        this.likesType = likesType == null ? null : likesType.trim();
    }

    public Long getLikeTypeId() {
        return likeTypeId;
    }

    public void setLikeTypeId(Long likeTypeId) {
        this.likeTypeId = likeTypeId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}