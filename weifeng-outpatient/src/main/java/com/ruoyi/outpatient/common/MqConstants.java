package com.ruoyi.outpatient.common;

/**
 * @author 伟峰
 * @date 2022/5/14
 * @description: 消息队列测试
 */
public class MqConstants {

    // 交换机
    public final static String CHAT_EXCHANGE="chat.exchange";

    //监听新增的队列
    public final static String INSERT_QUEUE="insert.queue";

    //监听删除的列队
    public final static String DELETE_QUEUE="delete.queue";


    public final static String INSERT_BLOGTOFANS_QUEUE="insert.blogtofans.queue";


//    两个routing_key

    // 新增routing_key：用于交换机和队列的搬定
    public final static String INSERT_ROUTING_KEY="insert.routing.key";
    // 删除routing_key
    public final static String DELETE_ROUTING_KEY="delete.routing.key";


    public final static String INSERT_BLOGTOFANS_ROUTING_KEY="insert.blogtofans.routing.key";


//    然后让他们在MqConfigurtion类中搬定




}
