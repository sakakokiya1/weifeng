package com.ruoyi.outpatient.mapper;

import com.ruoyi.outpatient.domain.Blog;
import com.ruoyi.outpatient.domain.Person;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IPersonLoginMapper {
    /**
     * 根据手机号查询用户信息
     * @param phonenumber 手机号
     * @return 用户信息
     */
    Person selectPersion(String phonenumber);


    /**
     * 插入个人信息
     * @param person
     */
    void insertPerson(Person person);


    /**
     * 通过personId查询个人信息
     * @param personId
     * @return person
     */
    Person selectMyInformation(Long personId);

    /**
     * 更新个人信息
     * @param person
     */
    void informationUpdate(Person person);

    /**
     * 按个人id查询个人blog列表 创建时间排序
     * @param personId
     * @return
     */

    /**
     * 关注我的粉丝
     * @return
     */
    List<Person> selectFansList(Long personId);


    /**
     * 我关注的博主列表
     * @return
     */
    List<Person> selectBlogerList(Long personId);

    /**
     * 查询用户信息
     * @param personId
     * @return
     */
    Person selectPerson(Long personId);

    /**
     * 粉丝总数
     * @return
     */
    int selectFansSum(Long personId);

    /**
     * 关注总数
     * @param personId
     * @return
     */
    int selectFollowSum(Long personId);


    /**
     * 通过关键字搜索用户列表
     * @param tagName
     * @return
     */
    List<Person> selectPersonListByTagName(String tagName);


    /**
     * 查找所有personid
     * @return
     */
    List<Long> selectPersonId();

    /**
     * 通过id集合查询person集合
     * @param collect
     * @return
     */
    List<Person> selectPersonList(List<Long> list);
}
