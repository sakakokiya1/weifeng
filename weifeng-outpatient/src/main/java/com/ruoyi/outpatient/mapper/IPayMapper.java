package com.ruoyi.outpatient.mapper;

import com.ruoyi.outpatient.domain.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 伟峰
 * @date 2022/5/11
 * @description:
 */
@Mapper
public interface IPayMapper {
    Order getOrderPay(String orderNumber);

}
