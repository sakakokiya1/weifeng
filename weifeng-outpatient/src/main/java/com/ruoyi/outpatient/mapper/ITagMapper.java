package com.ruoyi.outpatient.mapper;

import com.ruoyi.outpatient.domain.Blog;
import com.ruoyi.outpatient.domain.Tag;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 伟峰
 * @date 2022/4/23
 * @description: 标签dao
 */
@Mapper
public interface ITagMapper {




    /**
     * 插入tag基本信息
     * @param tag
     * @return
     */
    int insertTag(Tag tag);


    /**
     * 插入blogid外键
     * @param blogId
     * @param tagId
     * @return
     */
    int insertTagForBlogId(Long blogId,Long tagId);

    /**
     * 根据最新的tagid查询blog集合
     * @param tag id+tagName
     * @return blog列表
     */
    List<Tag> selectNewBlogListByTagId(Tag tag);

    /**
     * 根据最热的tagid查询blog集合（由UK匹配）
     * @param tagName id+tagName
     * @return blog列表+浏览量
     */
    List<Tag> selectBlogListByTagName(String tagName);


    /**
     * 通过blogid查询tag集合
     * @param blogId
     * @return
     */
    List<Tag> selectTagByBlogId(Long blogId);


    List<Tag> selectGTagList(Tag tag);

    /**
     * 后台根据id删除tag列表
     * @param tagIds
     * @return
     */
    int deleteTagByIds(Long[] tagIds);

    /**
     * 后台插入tag信息
     * @param tag
     * @return
     */
    int insertGTag(Tag tag);


    int updateGTag(Tag tag);
}
