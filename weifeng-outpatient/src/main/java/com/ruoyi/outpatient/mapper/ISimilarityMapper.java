package com.ruoyi.outpatient.mapper;

import com.ruoyi.outpatient.domain.Similarity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 伟峰
 * @date 2022/5/22
 * @description:
 */
@Mapper
public interface ISimilarityMapper {


    /**
     * 获取推荐列表id
     * @param loginPersonId
     * @return
     */
    List<Long> selectSimilarityId(Long loginPersonId);


    /**
     * 随机获取
     * @param randomId
     * @return
     */
    List<Long> selectBlogIdByRandom(Integer randomId);


    /**
     * 插入用户对应的相似度(定时任务)
     * @param doubles
     * @return
     */
    int insert(@Param("list")List<Similarity> doubles);
}
