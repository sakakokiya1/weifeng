package com.ruoyi.outpatient.mapper;

import com.ruoyi.outpatient.domain.Comment;
import com.ruoyi.outpatient.domain.CommentVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 伟峰
 * @date 2022/4/30
 * @description: 评论dao
 */
@Mapper
public interface ICommentMapper {

    /**
     * 通过分享ID查询所有的评论回复
     * @param blogId
     * @return
     */
    List<Comment> selectCommnetByBlogId(Long blogId);

    /**
     *  根据blogid查找点赞最多的评论列表 前10条记得分页
     * @param blogId
     * @return 评论列表
     */
    List<Comment> selectHotCommentByBlogId(Long blogId);


    /**
     * 根据blogid查找最新的评论
     * @param blogId
     * @return
     */
    List<Comment> selectNewsCommentByBlogId(Long blogId);

    /**
     * 根据commentId进行点赞
     * @param commentId
     * @return
     */
    long selectCommentLikes(Long commentId);


    List<Long> selectCommentId();

    /**
     * 对blog进行评论
     * @param comment 评论实体
     * @return
     */
    int insertComment(Comment comment);

    /**
     * 通过评论id查询评论信息
     * @param commentId
     * @return
     */
    Comment selectCommentById(Long commentId);
}
