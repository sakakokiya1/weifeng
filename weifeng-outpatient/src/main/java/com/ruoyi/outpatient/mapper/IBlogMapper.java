package com.ruoyi.outpatient.mapper;

import com.ruoyi.outpatient.domain.Blog;
import com.ruoyi.outpatient.domain.Person;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.ComponentScan;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 伟峰
 * @date 2022/4/20
 * @description: blog DAO
 */
@Mapper
public interface IBlogMapper {

    /**
     *  插入用户上传分享的信息
     * @param blog
     * @return int
     */
    int insertBlog(Blog blog);


    /**
     * 通过blogid查询出 用户+分享+标签的信息
     * @param blogId 分享id
     * @return blog实体类
     */
    Blog selectBlogDetails(Long blogId);


    List<Blog> selectBlogList(List<Long> list);

    /**
     * 通过personId查询个人分享文章 按时间排序
     * @param personId
     * @return
     */
    List<Blog> selectMyBlogList(Long personId);

    /**
     * 通过tagName找出访问量高的blog集合 (根据blog的访问量来模糊查询)
     * @param tagName
     * @return
     */
    List<Blog> selectBlogListByTagName(String tagName);

    List<Blog> selectAllTag();

    /**
     * 点赞加一
     * @param i
     * @return
     */
    Boolean updateLikes(int i);


    Boolean updateCollect(int i);


    Boolean updatecollect(int i);

    /**
     * 查找blog的总点赞数
     * @param blogId
     * @return
     */
    long selectBlogLikes(Long blogId);

    /**
     * 查询全表的id
     * @return idList
     */
    List<Long> selectBlogId();

    /**
     * 批量更新点赞总数
     * @param map key为blogid value为点赞总数
     * @return int
     */

//    UPDATE blog b SET b.`likes`=#{value} WHERE b.`blog_id`=#{key}


    /**
     * 定时任务 批量更新点赞信息
     * @param map
     * @return
     */
    int updateBlogLikes(@Param("map") HashMap<Long, Long> map);


    /**
     *通过id获取blog列表 按时间排序  map<id,location>
     * @param map
     * @return
     */
    List<Blog> selectNearbyId(@Param("map") HashMap<Long, String> map);

    /**
     * 、查询后台blog列表
     * @param blog
     * @return
     */
    List<Blog> selectGBlogList(Blog blog);

    /**
     * 后台根据id删除BLOG列表
     * @param blogIds
     * @return
     */
    int deleteBlogByIds(Long[] blogIds);

    /**
     * 后台插入blog信息
     * @param blog
     * @return
     */
    int insertGBlog(Blog blog);

    /**
     * 后台修改blog信息
     * @param blog
     * @return
     */
    int updateBlog(Blog blog);

    /**
     * 用户收藏总数
     * @param blogId
     * @return
     */
    long selectBlogCollects(Long blogId);


    int updateBlogCollects(@Param("map") HashMap<Long, Long> map);

    /**
     *
     * @param personId
     * @return
     */
    List<Blog> selectMyCollectList(Long personId);

    /**
     * 通过id查询blog按给定时间排序
     * @param ids
     * @return
     */
    List<Blog> selectBlogListByTime(@Param("list")List<Long> ids);

    /**
     * 个人笔记总数
     * @param loginPersonId
     * @return
     */
    long selectCountBlog(Long loginPersonId);

    /**
     * 查询用户推荐列表的id
     * @param loginPersonId 用户id
     * @return blogid列表
     */

}
