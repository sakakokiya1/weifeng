package com.ruoyi.outpatient.mapper;

import com.ruoyi.outpatient.domain.BlogTag;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 伟峰
 * @date 2022/4/25
 * @description: blog和tag关联外键
 */
@Mapper
public interface IBlogTagMapper {


    /**
     * 插入blog和tag外键
     * @param blogTag
     */
    void insertBlogTagById(BlogTag blogTag);

}
