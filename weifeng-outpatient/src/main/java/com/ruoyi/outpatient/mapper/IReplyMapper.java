package com.ruoyi.outpatient.mapper;

import com.ruoyi.outpatient.domain.Comment;
import com.ruoyi.outpatient.domain.CommentVo;
import com.ruoyi.outpatient.domain.Reply;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author 伟峰
 * @date 2022/5/15
 * @description: 回复DAO
 */
public interface IReplyMapper {

    /**
     * 根据评论列表id查询对应的回复列表
     * @param commentList 评论列表
     * @return 回复集合
     */
    List<Reply> selectReplyListByCommentId(@Param("list") List<CommentVo> commentVo);

    /**
     * 根据回复id查询点赞数
     * @param replyId
     * @return
     */
    long selectReplyLikes(Long replyId);

    List<Long> selectReplyId();


    /**
     * 插入回复
     * @param reply
     * @return
     */
    int insertReply(Reply reply);


    /**
     * 查找评论下的那条回复
     * @param replyId
     * @param commentId
     * @return
     */
    Reply selectReplyById(Long replyId, Long commentId);


    /**
     * 查找回复下的那条回复
     * @param replyId
     * @param commentId
     * @return
     */
    Reply selectReplyById2(Long replyId, Long commentId);
}
