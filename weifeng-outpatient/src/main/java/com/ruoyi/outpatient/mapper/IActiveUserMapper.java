package com.ruoyi.outpatient.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @author 伟峰
 * @date 2022/5/31
 * @description:
 */
@Mapper
public interface IActiveUserMapper {


    /**
     * 插入所有活跃用户
     * @param list
     * @return
     */
    int insert(@Param("list") List<Long> list);

    /**
     * 查询所有活跃用户id
     * @return
     */
    List<Long> selectAllId();

}
