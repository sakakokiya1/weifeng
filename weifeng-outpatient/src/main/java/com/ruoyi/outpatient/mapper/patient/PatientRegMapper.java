package com.ruoyi.outpatient.mapper.patient;


import com.ruoyi.outpatient.domain.PatientIdentity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 患者信息处理dao
 */
@Mapper
public interface PatientRegMapper {
    /**
     *  插入患者身份证信息
     * @param identity 身份信息
     * @return int
     */
    int insertIdentity(PatientIdentity identity);

    /**
     *  患者用户名进行认证查询
     * @param username
     * @return 患者认证信息
     */
   PatientIdentity selectPatientBymedicalCard(String username);
}
