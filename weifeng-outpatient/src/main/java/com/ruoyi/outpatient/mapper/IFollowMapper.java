package com.ruoyi.outpatient.mapper;

import com.ruoyi.outpatient.domain.Follow;
import com.ruoyi.outpatient.domain.Person;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author 伟峰
 * @date 2022/5/4
 * @description:
 */
@Mapper
public interface IFollowMapper {

    Boolean insert(Follow follow);

    /**
     * 将我关注的博主删除
     * @param follow
     */
    Boolean deleteFollow(Follow follow);

    /**
     * 查询用户是否关注
     * @param followPersonId
     * @return
     */
    Integer selectIsFollow(Long personId,Long followPersonId);

    /**
     * 查询我的粉丝列表id
     * @param loginPersonId
     * @return
     */
    List<Long> selectMyFansList(Long loginPersonId);
    /**
     * 关注我的粉丝列表
     * @return
     */

}
