//package com.ruoyi.outpatient.core;
//
//import com.ruoyi.common.core.domain.model.LoginUser;
//import com.ruoyi.common.utils.SecurityUtils;
//import com.ruoyi.common.utils.StringUtils;
//import com.ruoyi.framework.web.service.TokenService;
//import com.ruoyi.outpatient.service.patient.impl.PatientTokenService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//
///** OncePerRequestFilter
// * 该过滤器能够使request在单次请求中只过滤一次，什么意思呢？我们知道过滤器可以配置拦截多个请求路径的，
// * 假如我们的一次请求经过了该过滤器然后到达servlet，但是servlet又进行了转发，
// * 这时转发的路径又要被该过滤器过滤，那么我们的过滤器就会在用户的一次请求中执行两次，
// * 有时这并不是我们想要的。这时我们就可以继承OncePerRequestFilter过滤器并实现doFilterInternal方法，
// * 在这个方法里写我们自己的连接器的处理逻辑。
// */
//
///**
// * Demo class
// *
// * @author weifeng
// * @date 2022/4/4
// */
//
////@Component
//@Configuration
//public class PatientJwtAuthenticationTokenFilter extends OncePerRequestFilter {
//
////public class PatientJwtAuthenticationTokenFilter {
//
//    @Autowired
//    private PatientTokenService tokenService;
//
//
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//
//        //通过请求获取token在redis中的封装信息
//
////        LoginPatient patient = tokenService.getLoginUser(request);
//        LoginPatient loginPatient = tokenService.getLoginPatient(request);
//
////        如果封装对象不为空 且 认证了
//        if (StringUtils.isNotNull(loginPatient) && StringUtils.isNull(SecurityUtils.getAuthentication()))
//        {
//            tokenService.verifyToken(loginPatient);
////            封装认证信息到Authentication实现类中
//            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
//            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
////            将认证信息放到security上下文环境中
//            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
//        }
////       如果token取出的对象信息为空 则过滤
//        filterChain.doFilter(request, response);
//
//
//
//    }
//}
