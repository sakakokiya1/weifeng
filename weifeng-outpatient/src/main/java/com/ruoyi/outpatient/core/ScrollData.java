package com.ruoyi.outpatient.core;

import java.util.List;

/**
 * @author 伟峰
 * @date 2022/6/12
 * @description: 封装滚动分页
 */

public class ScrollData {

    private List<?> list;

    private Long minTime;

    private Long offset;

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }

    public Long getMinTime() {
        return minTime;
    }

    public void setMinTime(Long minTime) {
        this.minTime = minTime;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }
}
