package com.ruoyi.outpatient.core;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.StringUtils;
import io.jsonwebtoken.Claims;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 伟峰
 * @date 2022/5/16
 * @description: 安全配置类
 */
@Configuration
public class SecurityUtils {

    /**
     *  在security上下文获取person信息
     * @return
     */
    public LoginPatient getLoginPerson()
    {

        LoginPatient loginPatient=(LoginPatient)  SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return loginPatient;
    }


    /**
     * 在security上下文获取person id
     * @return
     */
    public Long getLoginPersonId()
    {

        LoginPatient loginPatient=(LoginPatient)  SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long personId = loginPatient.getPerson().getPersonId();
        return personId;
    }








}
