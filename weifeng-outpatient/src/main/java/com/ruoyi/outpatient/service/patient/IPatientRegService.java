package com.ruoyi.outpatient.service.patient;

import com.ruoyi.outpatient.domain.PatientIdentity;

public interface IPatientRegService {
    /**插入患者身份证信息 */
    PatientIdentity insertPatientIdentity(String identityCard, String patientName);

    /**
     * 患者登录认证
     * @param medicalCard 诊疗号
     * @param medicalPassword 诊疗卡密码
     */
    String patientLogin(String medicalCard, String medicalPassword);
}
