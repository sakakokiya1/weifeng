package com.ruoyi.outpatient.service;

import com.ruoyi.outpatient.domain.PayEnpity;

/**
 * @author 伟峰
 * @date 2022/5/11
 * @description:
 */
public interface IPayService {

//    获取当前订单的支付信息
    PayEnpity getOrderPay(String orderNumber);
}
