package com.ruoyi.outpatient.service.impl;

import com.ruoyi.outpatient.domain.Order;
import com.ruoyi.outpatient.domain.PayEnpity;
import com.ruoyi.outpatient.mapper.IPayMapper;
import com.ruoyi.outpatient.service.IPayService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author 伟峰
 * @date 2022/5/11
 * @description:
 */
@Service
public class PayServiceImpl  implements IPayService {

    @Autowired
    private IPayMapper payMapper;

//    获取当前订单的支付信息
    @Override
    public PayEnpity getOrderPay(String orderNumber) {
        PayEnpity payEnpity=new PayEnpity();

//        Order order=payMapper.getOrderPay(orderNumber);
//        BigDecimal bigDecimal = order.getPayAmount().setScale(2);
//
//
//        payEnpity.setTotal_amount(bigDecimal.toString());
//        payEnpity.setOut_trade_no(order.getOrderNumber());
        BigDecimal bigDecimal=new BigDecimal(22.22);
        payEnpity.setProduct_code("432423423423423");
        payEnpity.setTotal_amount(bigDecimal.toString());
        payEnpity.setOut_trade_no("202002131259194851227819517812478954");
        payEnpity.setSubject("第一");
        payEnpity.setBody("需要描述吗");
        return payEnpity;
    }
}
