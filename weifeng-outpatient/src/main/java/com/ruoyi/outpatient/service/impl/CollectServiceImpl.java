package com.ruoyi.outpatient.service.impl;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.outpatient.core.SecurityUtils;
import com.ruoyi.outpatient.mapper.IBlogMapper;
import com.ruoyi.outpatient.service.ICollectService;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author 伟峰
 * @date 2022/6/11
 * @description:
 */
@Service
public class CollectServiceImpl implements ICollectService {

    @Autowired
    private SecurityUtils securityUtils;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private IBlogMapper blogMapper;
    /**
     * 用户收藏blog
     * @param blogId
     * @return
     */
    @Override
    public AjaxResult collectBlog(Long blogId) {

        //        1.获取收藏的登录用户
        Long loginPersonId = securityUtils.getLoginPersonId();
        //        统计互动数（点赞+收藏+评论）
        redisCache.setCacheHyperLogLog("statistics:interaction:"+loginPersonId, UUID.randomUUID());

//        2.判断用户是否点收藏 (在blog中 看有没有该用户id) O(1)
        Object mapFieldValue = redisCache.getMapFieldValue("collects:blog", loginPersonId + ":" + blogId);

        // TODO: 2022/6/11
//        "collects:blog"设为永久key 只要定时同步数据库就行了,不需要删除key（需要使用AOF或RDB持久化）
//        blog收藏总数定时同步数据库,有无差也不怕


//        3. 只要hash不为空,并且不为0  说明为1即点收藏了
        if (StringUtils.isNotNull(mapFieldValue) && (Integer.parseInt(mapFieldValue.toString()) != 0) ) {

            //            blog收藏总数-1
            Long aLong = redisCache.setCacheDecrement("blog:collectSum:" + blogId);
            if (StringUtils.isNotNull(aLong)){
                //            当blog收藏总数>-1
                if (aLong > -1) {
//                redisCache.setCacheMapValue("likes:blog", loginPersonId + ":" + blogId, 0);
//                直接删除hash字段,表示取消收藏
                    redisCache.delCacheMapField("collects:blog", loginPersonId + ":" + blogId);
                }

                //                最后统计下该用户所有blog收藏总数
//            redisCache.setCacheHyperLogLog("person:viewTotal:" + loginPersonId);
                Object cacheObject = redisCache.getCacheObject("blog:collectSum:"+blogId);
                return AjaxResult.success("取消收藏成功").put("isCollects", false) //返回收藏状态 收藏总数
                        .put("collectSum",Long.valueOf(cacheObject.toString())==null ? 0:Long.valueOf(cacheObject.toString()));
            }else {
                //                去数据库查询blog总数
                long l = blogMapper.selectBlogCollects(blogId);
                redisCache.setCacheObject("blog:collectSum:" + blogId,l);

                //            当blog收藏总数>-1
                if (l > -1) {
//                redisCache.setCacheMapValue("likes:blog", loginPersonId + ":" + blogId, 0);
//                直接删除hash字段,表示取消收藏
                    redisCache.delCacheMapField("collects:blog", loginPersonId + ":" + blogId);
                }
                //                最后统计下该用户所有blog收藏总数
//            redisCache.setCacheHyperLogLog("person:viewTotal:" + loginPersonId);
                Object cacheObject = redisCache.getCacheObject("blog:collectSum:"+blogId);
                return AjaxResult.success("取消收藏成功").put("isCollects", false) //返回收藏状态 收藏总数
                        .put("collectSum",Long.valueOf(cacheObject.toString())==null ? 0:Long.valueOf(cacheObject.toString()));
            }



        }  //4. 只要hash不为空,并且不为0  说明为1即点过赞了
        else {

//            看key是否存储,不再去数据库  (不需要判断，因为rabbit几个小时刷新一次)
//            boolean exists = redisCache.exists("blog:likesPlusOne:" + blogId);

//             blog收藏总数+1
            Long aLong = redisCache.setCacheIncrement("blog:collectSum:" + blogId);

            if (StringUtils.isNotNull(aLong)){
                //            将给用户收藏放到hash 表示收藏过
                if (aLong > -1) {
//                添加hash字段状态为1,表示进行收藏
                    redisCache.setCacheMapValue("collects:blog", loginPersonId + ":" + blogId, 1);
                }

//                最后统计下该用户所有blog收藏总数
//            redisCache.setCacheHyperLogLog("person:viewTotal:" + loginPersonId);
                Object cacheObject = redisCache.getCacheObject("blog:collectSum:"+blogId);
                return AjaxResult.success("进行收藏成功").put("isCollects", true) //返回收藏状态 收藏总数
                        .put("collectSum",Long.valueOf(cacheObject.toString())==null ? 0:Long.valueOf(cacheObject.toString()));

            }else {
//                去数据库查询blog总数
                long l = blogMapper.selectBlogCollects(blogId);
                redisCache.setCacheObject("blog:collectSum:" + blogId,l);

                //            将给用户收藏放到hash 表示收藏过
                if (l > -1) {
//                添加hash字段状态为1,表示进行收藏
                    redisCache.setCacheMapValue("collects:blog", loginPersonId + ":" + blogId, 1);
                }

//                最后统计下该用户所有blog收藏总数
//            redisCache.setCacheHyperLogLog("person:viewTotal:" + loginPersonId);
                Object cacheObject = redisCache.getCacheObject("blog:collectSum:"+blogId);
                return AjaxResult.success("进行收藏成功").put("isCollects", true) //返回收藏状态 收藏总数
                        .put("collectSum",Long.valueOf(cacheObject.toString())==null ? 0:Long.valueOf(cacheObject.toString()));
            }

        }

    }
}
