package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;

/**
 * @author 伟峰
 * @date 2022/5/4
 * @description:
 */
public interface IFollowService {

    /**
     * 进行关注或取消关注
     * @param followPersonId 用户id
     * @param bool true为关注，false为取关
     * @return
     */
    AjaxResult updateFollowByPersonId(Long followPersonId, Boolean bool);

    /**
     * 判断是否关注过
     * @param followPersonId 用户id
     * @return
     */
    AjaxResult isfollow(Long followPersonId);

    /**
     * 关注我的粉丝列表
     * @return
     */
    AjaxResult fansList();

    /**
     * 我关注的博主列表
     * @return
     */
    AjaxResult blogerList();


    /**
     * 共同关注列表
     * @param id
     * @return
     */
    AjaxResult commonList(Long id);

    /**
     * 消息推送到粉丝
     * @param lastTime 本次要查询开始时间,也是上次结束时间
     * @param offset 查询偏移量 如果默认0即第一次查询
     * @return
     */
    AjaxResult messageList(Long lastTime, Long offset);

}
