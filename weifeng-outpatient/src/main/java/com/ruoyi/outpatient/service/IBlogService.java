package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.domain.Blog;
import com.ruoyi.outpatient.domain.Person;

import java.util.List;

/**
 * @author 伟峰
 * @date 2022/4/20
 * @description: 分享业务处理
 */
public interface IBlogService {


    /**
     * 查询详情信息
     * @param blogId
     * @return
     */
    AjaxResult selectBlogDetails(Long blogId);

    /**
     * 对blog进行点赞
     * @param blogId 用户点赞
     * @return
     */
    AjaxResult likeBlog(Long blogId);

    /**
     * 首页推介列表
     */
    List<Blog> selectBlogList();

    /**
     * 附近的人
     * @param latitude 经度
     * @param latitude1 纬度
     * @return blogList
     */
    List<Blog> selectNearbyBlog(Double longitude,Double latitude);

    /**
     * 对blog进行收藏
     * @param blogId
     * @return
     */
    AjaxResult likeCollect(Long blogId);


    /**
     * 个人发布笔记管理中心
     * @return
     */
    AjaxResult personBlogManagement();


    /**
     * 后台 查询blog列表
     * @param blog
     * @return
     */
    List<Blog> selectGBlogList(Blog blog);

    /**
     *
     * 后台根据id删除BLOG列表
     * @param blogIds
     * @return
     */
    int deleteBlogByIds(Long[] blogIds);

    /**
     * 后台插入blog信息
     * @param blog
     * @return
     */
    int insertGBlog(Blog blog);

    /**
     * 后台修改blog信息
     * @param blog
     * @return
     */
    int updateBlog(Blog blog);
}
