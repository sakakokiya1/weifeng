package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;

/**
 * @author 伟峰
 * @date 2022/6/12
 * @description:
 */
public interface IStatisticsService {

    /**
     * 统计个人数据 新增粉丝 主页访客 观看数 互动数
     * @return
     */
    AjaxResult personDateSum();

    /**
     * 统计当前笔记收藏点赞
     * @return
     */
    AjaxResult bloglikecollectAllSum();



}
