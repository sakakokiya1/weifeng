package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.domain.Reply;

/**
 * @author 伟峰
 * @date 2022/5/19
 * @description: 回复service
 *
 */
public interface IReplyService {

    /**
     * 对回复进行点赞
     * @param replyId 回复id
     * @return true
     */
    AjaxResult likeReply(Long replyId);


    /**
     * 对评论进行回复
     * @param reply
     * @return
     */
    AjaxResult addReply(Reply reply);
}
