package com.ruoyi.outpatient.service.impl;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.outpatient.core.SecurityUtils;
import com.ruoyi.outpatient.domain.Comment;
import com.ruoyi.outpatient.domain.Reply;
import com.ruoyi.outpatient.mapper.IReplyMapper;
import com.ruoyi.outpatient.service.IReplyService;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author 伟峰
 * @date 2022/5/19
 * @description:
 */
@Service
public class ReplyServiceImpl implements IReplyService {


    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SecurityUtils securityUtils;


    @Autowired
    private IReplyMapper replyMapper;
    /**
     * 对回复进行点赞
     * @param replyId 回复id
     * @return true
     */
    @Override
    public AjaxResult likeReply(Long replyId) {


        //        1.获取点赞的登录用户
        Long loginPersonId = securityUtils.getLoginPersonId();

//        2.判断用户是否点赞过 (在comment中 看有没有该用户id) O(1)
        Boolean isMember = redisCache.isMemberSet("reply:likes:" + replyId,loginPersonId);


        if (BooleanUtils.isFalse(isMember)){

//            看键是否存在
            boolean exists = redisCache.exists("reply:likesPlusOne:" + replyId);

            if (exists==true){
//                说明set中的key没过期
                Long aLong = redisCache.setCacheIncrement("reply:likesPlusOne:" + replyId);

//            将 给用户点赞 放到set 表示点赞过
                if (aLong>-1){
                    redisCache.setCacheSets("reply:likes:" + replyId,loginPersonId);
                }

                //                最后统计下该用户所有点赞总数
                redisCache.setCacheHyperLogLog("person:viewTotal:"+loginPersonId);
                //        统计互动数（点赞+收藏+评论）
                redisCache.setCacheHyperLogLog("statistics:interaction:"+loginPersonId, UUID.randomUUID());
                return AjaxResult.success("回复点赞成功").put("likesSum",aLong);
            }

//                说明redis统计已经过期了,要去数据库拿
            long likesSum=replyMapper.selectReplyLikes(replyId);
//            将数据库更新到redis中 且要加一   设置15天后过期,然后定时任务在过期时刷新到数据库
            redisCache.setCacheObject("reply:likes:" + replyId,likesSum+1,15, TimeUnit.DAYS);


            //                最后统计下该用户所有点赞总数
            redisCache.setCacheHyperLogLog("person:viewTotal:"+loginPersonId);
            //        统计互动数（点赞+收藏+评论）
            redisCache.setCacheHyperLogLog("statistics:interaction:"+loginPersonId, UUID.randomUUID());
            return AjaxResult.success("回复进行点赞成功").put("likesSum",likesSum);

        }else {

            boolean exists = redisCache.exists("reply:likesPlusOne:" + replyId);
            if (exists==true){
//                说明set中的key没过期
                //            redis点赞数-1
                Long aLong = redisCache.setCacheDecrement("reply:likesPlusOne:" + replyId);

//            将给用户点赞放到set 表示点赞过
                if (aLong>-1){
                    redisCache.setRemove("reply:likes:" + replyId,loginPersonId);
                }
                return AjaxResult.success("回复取消点赞成功").put("likesSum",aLong);
            }

//                说明redis统计已经过期了,要去数据库拿
            long likesSum=replyMapper.selectReplyLikes(replyId);
//            将数据库更新到redis中且 减1     设置15天后过期,然后定时任务在过期时刷新到数据库
            redisCache.setCacheObject("reply:likes:" + replyId,likesSum-1,15,TimeUnit.DAYS);

            return AjaxResult.success("回复取消点赞成功").put("likesSum",likesSum);
        }
    }


    /**
     * 对评论进行回复
     * @param reply
     * @return
     */
    @Override
    public AjaxResult addReply(Reply reply) {
        //        1.获取登录用户
        Long loginPersonId = securityUtils.getLoginPersonId();


        //        统计互动数（点赞+收藏+评论）
        redisCache.setCacheHyperLogLog("statistics:interaction:"+loginPersonId, UUID.randomUUID());

//        comment_id  reply_content reply_type replygoal_id my_id

//        comment.setCreateTime(new Date());
        reply.setMyId(loginPersonId);
        int i=replyMapper.insertReply(reply);
//        Comment c=replyMapper.selectReplyById(reply.getCommentId());

        if (reply.getReplyType().equals('1')){
                    Reply r=replyMapper.selectReplyById(reply.getReplyId(),reply.getCommentId());

                    return AjaxResult.success().put("reply",r);
        }else {

            Reply r=replyMapper.selectReplyById2(reply.getReplyId(),reply.getReplygoalId());
            return AjaxResult.success().put("reply",r);
        }



    }


}
