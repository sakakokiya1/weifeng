package com.ruoyi.outpatient.service.impl;

import cn.hutool.core.date.DateUtil;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.outpatient.core.LoginPatient;
import com.ruoyi.outpatient.core.SecurityUtils;
import com.ruoyi.outpatient.service.ISignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

/**
 * @author 伟峰
 * @date 2022/5/29
 * @description:
 */
@Service
public class SignServiceImpl implements ISignService {


    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SecurityUtils securityUtils;
    /**
     * 用户进行每日签到
     * @return
     */
    @Override
    public Boolean setSign() {

        Long loginPersonId = securityUtils.getLoginPersonId();

        String today= DateUtil.today();
        String[] split = today.split("-");
//        不能同时setbin多个,要使用管道   (signperson:6:2022:03,19,1)
        Boolean aBoolean = redisCache.setBig("signperson:" + loginPersonId + ":" + split[0] + ":" + split[1], Long.valueOf(split[2]) - 1, true);
        return aBoolean;



////        1.获取用户id
//        LoginPatient principal =(LoginPatient) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//
////        2.获取当前日期月份yyyy-dd
//        String dates = DateUtils.getDates();
//
//
////        3.拼接key为了获取 bigmap
//        String key= principal.getPerson().getPersonId()+":"+ dates;
////        4.获取今天是第几天，来存入bigmap
//        LocalDate now = LocalDate.now();
//        int day = now.getDayOfMonth(); //获取当日是几号
//
////     5.存储到bigmap   key：(用户ID：2022-03)  value:12号 true
//        redisCache.setBig(key,day,true);



    }


    /**
     * 用户每个月连续签到天数
     * @return
     */
    @Override
    public int getContinuous() {

        Long loginPersonId = securityUtils.getLoginPersonId();

        String today= DateUtil.today();
        String[] split = today.split("-");
//        不能同时setbin多个,要使用管道

//        long bigsCount = redisCache.getBigsCount("signperson:" + loginPersonId + ":" + split[0] + ":" + split[1], Integer.parseInt(split[2]) - 1, 0);
        long bigsCount = redisCache.getBigsCount("signActiveUser:" + loginPersonId + ":" + split[0] + ":" + split[1], Integer.parseInt(split[2]) - 1, 0);


        int sum=0;
        while (true) {

//            让这数字与1做与运算,得到数字的最后一个bit位
            if ((bigsCount & 1) ==0){
//                如果为0说明为签到,断签了 直接返回
                break;
            }else {
//                不为0说明签到了,就一直统计直到今天到这个月1号都统计完
                sum++;
            }
            bigsCount >>>= 1;
        }

        return sum;
    }


    /**
     * 显示签到天数
     * @return
     */
    @Override
    public AjaxResult showSign () {
        Long loginPersonId = securityUtils.getLoginPersonId();

        String today= DateUtil.today(); //2022-05-13
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int dateSum=0;
        try {
            dateSum=getDaysOfMonth(sdf.parse(today)); //生成每个月有多少天
            System.out.println(getDaysOfMonth(sdf.parse("2022-05-13")));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] split = today.split("-");
//        不能同时setbin多个,要使用管道
//        long bigsCount = redisCache.getBigsCount("signperson:" + loginPersonId + ":" + split[0] + ":" + split[1], Integer.parseInt(split[2]) - 1, 0);
        long bigsCount = redisCache.getBigsCount("signActiveUser:" + loginPersonId + ":" + split[0] + ":" + split[1], dateSum ,0);
//        start和end包头不包尾


//        使用数组来显示签到的天数
            Integer[] calendar=new Integer[dateSum];
            for (int i=0;i<dateSum;i++){
                if ((bigsCount & 1)==0){  //与0就下标添加0
                    calendar[dateSum-i-1]=0;
                    bigsCount >>>= 1;
                }else {
                    calendar[dateSum-i-1]=1; //与到1就下标添加1
                    bigsCount >>>= 1;
                }
            }
            return AjaxResult.success().put("array",calendar);
    }

    //生成每个月有多少天
    public static int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取这个月的签到
     * @return
     */
//    @Override
//    public int getContinuouss() {
//
//        Long loginPersonId = securityUtils.getLoginPersonId();
//
//        String today= DateUtil.today();
//        String[] split = today.split("-");
////        不能同时setbin多个,要使用管道
//
//        long bigsCount = redisCache.getBigsCount("signperson:" + loginPersonId + ":" + split[0] + ":" + split[1], Integer.parseInt(split[2]) - 1, 0);
//
//
//        int sum=0;
//        while (true) {
//
////            让这数字与1做与运算,得到数字的最后一个bit位
//            if ((bigsCount & 1) ==0){
////                如果为0说明为签到,断签了 直接返回
//                break;
//            }else {
//
////                不为0说明签到了,就一直统计直到今天到这个月1号都统计完
//                sum++;
//            }
//            bigsCount >>>= 1;
//        }
//
//        return sum;
//    }


}
