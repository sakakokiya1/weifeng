//package com.ruoyi.outpatient.service.patient.impl;
//
//
//
//import com.ruoyi.common.core.domain.model.LoginUser;
//import com.ruoyi.common.exception.ServiceException;
//import com.ruoyi.common.utils.StringUtils;
//import com.ruoyi.outpatient.core.LoginPatient;
//import com.ruoyi.outpatient.domain.PatientIdentity;
//import com.ruoyi.outpatient.mapper.patient.PatientRegMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//@Qualifier("PatientDetailsServiceImpl")
//public class PatientDetailsServiceImpl implements UserDetailsService {
//    @Autowired
//    private PatientRegMapper regMapper;
//
//
//    /**
//     *  患者容器中用户名与数据库认证
//     * @param medicalCard
//     * @return loginuser
//     * @throws UsernameNotFoundException
//     */
//    @Override
//    public UserDetails loadUserByUsername(String medicalCard) throws UsernameNotFoundException {
////        查询到返回，他会自己进行认证
//
//
//        PatientIdentity identity=regMapper.selectPatientBymedicalCard(medicalCard);
//
//        if (StringUtils.isNull(identity))
//        {
//            throw new ServiceException("该用户不存在");
//        }
//        if (StringUtils.isNull(identity.getIdentityCard()))
//        {
//            throw new ServiceException("请先用身份证认证");
//        }
//
//
////        患者权限设置
//
//
////        先存储信息后期加权限
//        return new LoginPatient(identity);
//    }
//}
