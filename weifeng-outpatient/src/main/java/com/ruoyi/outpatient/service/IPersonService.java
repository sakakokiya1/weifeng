package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.domain.Person;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 伟峰
 * @date 2022/5/4
 * @description:
 */
public interface IPersonService {
    /**
     * 通过personId查询个人信息
     * @param personId
     * @return
     */
    AjaxResult selectMyInformation(Long personId);

    /**
     *通过personId查询个人分享文章
     * @param personId
     * @return
     */
    AjaxResult selectMyBlogList(Long personId);



    /**
     * 更新用户信息
     * @param person
     * @return
     */
    AjaxResult informationUpdate(Person person);


    /**
     * 用户签到实现
     * @return void
     */
    AjaxResult insertSign();

    /**
     * 获取短信验证码
     * @param phonenumber 手机号
     * @return 短信验证码
     */
    AjaxResult selectVerification(String phonenumber);

    /**
     *
     * @return
     */
    AjaxResult selectLikeAndCollect();

    /**
     *  通过关键字搜索用户列表
     * @param tagName
     * @return
     */
    AjaxResult selectPersonListByTagName(String tagName);




    /**
     *通过personId查询个人收藏笔记
     * @param personId
     * @return
     */
    AjaxResult selectMyCollectList(Long loginPersonId);
}
