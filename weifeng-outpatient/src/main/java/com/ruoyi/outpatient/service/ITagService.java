package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.domain.Tag;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 伟峰
 * @date 2022/4/25
 * @description:
 */

public interface ITagService {
    /**
     * 从标签名查询最新的list
     * @param tag 标签名
     * @return 列表
     */
    AjaxResult selectNewTagList(Tag tag);

    /**
     * 通过tag检索最热门
     * @param tag tagId+tagName
     * @return list
     */
    AjaxResult selectBlogListByTagName(String tagName);

    /**
     * 通过标签搜索 综合排序
     * @param tagName
     * @return
     */
    AjaxResult selectComprehenBlogList(String tagName);

    /**
     * 后台 查询tag列表
     * @param tag
     * @return
     */
    List<Tag> selectGTagList(Tag tag);

    /**
     * 后台根据id删除tag列表
     * @param tagIds
     * @return
     */
    int deleteGTagByIds(Long[] tagIds);

    /**
     * 后台删除tag
     * @param tag
     * @return
     */
    int insertGTag(Tag tag);

    /**
     * 后台更新tag
     * @param tag
     * @return
     */
    int updateGTag(Tag tag);
}
