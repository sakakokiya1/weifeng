package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.domain.Comment;

/**
 * @author 伟峰
 * @date 2022/4/30
 * @description:
 */
public interface ICommentService {

    /**
     * 给评论进行点赞
     * @param blogId 分享id
     * @param commentId 评论id
     * @return true
     */
    AjaxResult likeComment( Long commentId);


    /**
     * 刷新更多的评论
     * @param blogId
     * @param createTime
     * @return
     */
    AjaxResult selectCommentList(Long blogId, Long createTime);

    /**
     * 对blog进行评论
     * @param blogId
     * @return
     */
    AjaxResult addComment(Comment comment);

}
