package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;

/**
 * @author 伟峰
 * @date 2022/6/11
 * @description:
 */
public interface ICollectService {
    /**
     * 用户收藏blog
     * @param blogId
     * @return
     */
    AjaxResult collectBlog(Long blogId);
}
