package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.domain.Patient;
import com.ruoyi.system.domain.KsDept;


import java.util.List;

public interface IOptRegistrationService {




    /**
     * 获取当前的挂号科室列表
     * @return
     */
    List<Patient> selectOptRegistrationList();


    /**
     * 获取当前的挂号科室列表
     * @param ksDept
     * @return
     */
    AjaxResult selectKsDeptList(KsDept ksDept);
}
