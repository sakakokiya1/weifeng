package com.ruoyi.outpatient.service.patient.impl;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.PersonConstants;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.ip.AddressUtils;
import com.ruoyi.common.utils.ip.IpUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.outpatient.core.LoginPatient;
import com.ruoyi.outpatient.domain.Person;
import eu.bitwalker.useragentutils.UserAgent;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * token验证处理
 *
 * @author ruoyi
 */
@Component
public class PatientTokenService
{
    // 令牌自定义标识
    @Value("${token.header}")
    private String header;

    // 令牌秘钥
    @Value("${token.secret}")
    private String secret;


    // 令牌有效期（默认30分钟）
    @Value("${token.expireTime}")
    private int expireTime;

    protected static final long MILLIS_SECOND = 1000;

    protected static final long MILLIS_MINUTE = 60 * MILLIS_SECOND;

    private static final Long MILLIS_MINUTE_TEN = 20 * 60 * 1000L;

    @Autowired
    private RedisCache redisCache;

    /**
     * 获取用户身份信息
     *
     * @return 用户信息
     */
    public LoginPatient getLoginPatient(HttpServletRequest request)
    {

        // 获取请求携带的令牌
        String token = getToken(request);
        if (StringUtils.isNotEmpty(token))
        {
            try
            {
                Claims claims = parseToken(token);
                // 解析对应的权限以及用户信息
                String uuid = (String) claims.get(Constants.LOGIN_Patient_KEY);
                String patientKey = getTokenKey(uuid);
                LoginPatient patient = redisCache.getCacheObject(patientKey);
                return patient;
            }
            catch (Exception e)
            {
            }
        }
        return null;
    }






//    使用redis的map来存储用户信息
//    public LoginPatient getLoginPatients(HttpServletRequest request)
//    {
//        // 获取请求携带的令牌
//        String token = getToken(request);
//        if (StringUtils.isNotEmpty(token))
//        {
//            try
//            {
//                Claims claims = parseToken(token);
//                // 解析对应的权限以及用户信息
//                String uuid = (String) claims.get(Constants.LOGIN_Patient_KEY);
//                String patientKey = getTokenKey(uuid);
////                LoginPatient patient = redisCache.getCacheObject(patientKey);
//                LoginPatient patient = redisCache.getCacheMap(patientKey);
//                return patient;
//            }
//            catch (Exception e)
//            {
//            }
//        }
//        return null;
//    }





    /**
     * 获取用户id
     * @param request
     * @return
     */
    public Long getLoginPatientId(HttpServletRequest request)
    {
        // 获取请求携带的令牌
        String token = getToken(request);
        if (StringUtils.isNotEmpty(token))
        {
            try
            {
                Claims claims = parseToken(token);
                // 解析对应的权限以及用户信息
                String uuid = (String) claims.get(Constants.LOGIN_Patient_KEY);
                String patientKey = getTokenKey(uuid);
                LoginPatient patient = redisCache.getCacheObject(patientKey);
                Long personId = patient.getPerson().getPersonId();
                return personId;
            }
            catch (Exception e)
            {
            }
        }
        return null;
    }





    /**
     * 设置用户身份信息
     */
    public void setLoginPatient(LoginPatient loginPatient)
    {
        if (StringUtils.isNotNull(loginPatient) && StringUtils.isNotEmpty(loginPatient.getToken()))
        {
//            刷新令牌时间
            refreshToken(loginPatient);
        }
    }

    /**
     * 删除用户身份信息
     */
    public void delLoginPatient(String token)
    {
        if (StringUtils.isNotEmpty(token))
        {
            String patientKey = getTokenKey(token);
            redisCache.deleteObject(patientKey);
        }
    }

    /**
     * 获取token
     * @param loginPatient 用户信息
     * @return token
     */
    public String createToken(LoginPatient loginPatient)
    {
        String token = IdUtils.fastUUID();
        loginPatient.setToken(token);
        setLoginPatient(loginPatient);
        refreshToken(loginPatient);

        Map<String, Object> claims = new HashMap<>();
        claims.put(Constants.LOGIN_Patient_KEY, token);
        return createToken(claims);
    }



    /**
     * 验证令牌有效期，相差不足20分钟，自动刷新缓存
     *
     * @param loginPatient
     * @return 令牌
     */
    public void verifyToken(LoginPatient loginPatient)
    {
        long expireTime = loginPatient.getExpireTime();
        long currentTime = System.currentTimeMillis();
        if (expireTime - currentTime <= MILLIS_MINUTE_TEN)
        {
            refreshToken(loginPatient);
        }
    }


    /**
     * 刷新令牌有效期
     *
     * @param loginPatient 登录信息
     */
    public void refreshToken(LoginPatient loginPatient)
    {
        loginPatient.setLoginTime(System.currentTimeMillis()); //刷新当前时间

        loginPatient.setExpireTime(loginPatient.getLoginTime() + expireTime * MILLIS_MINUTE);
        // 根据uuid将loginUser缓存
        String patientKey = getTokenKey(loginPatient.getToken()); //uuid+token当做key
//        重新刷新缓存
        redisCache.setCacheObject(patientKey, loginPatient, expireTime, TimeUnit.MINUTES);

    }



    /**
     * 设置用户代理信息
     *
     * @param loginUser 登录信息
     */
    public void setUserAgent(LoginPatient loginPatient)
    {
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        String ip = IpUtils.getIpAddr(ServletUtils.getRequest());
//        loginUser.setIpaddr(ip);
//        loginUser.setLoginLocation(AddressUtils.getRealAddressByIP(ip));
//        loginUser.setBrowser(userAgent.getBrowser().getName());
//        loginUser.setOs(userAgent.getOperatingSystem().getName());
    }

    /**
     * 从数据声明生成令牌
     *
     * @param claims 数据声明
     * @return 令牌
     */
    private String createToken(Map<String, Object> claims)
    {

        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
        return token;
    }

    /**
     * 从令牌中获取数据声明
     *
     * @param token 令牌
     * @return 数据声明
     */
    private Claims parseToken(String token)
    {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * 从令牌中获取用户名
     *
     * @param token 令牌
     * @return 用户名
     */
    public String getPatientnameFromToken(String token)
    {
        Claims claims = parseToken(token);
        return claims.getSubject();
    }

    /**
     * 获取请求token
     *
     * @param request
     * @return token
     */
    private String getToken(HttpServletRequest request)
    {
        String token = request.getHeader(header);
        if (StringUtils.isNotEmpty(token) && token.startsWith(Constants.TOKEN_PREFIX))
        {
            token = token.replace(Constants.TOKEN_PREFIX, "");
        }
        return token;
    }

    private String getTokenKey(String uuid)
    {

        return PersonConstants.LOGIN_TOKEN_KEY + uuid;
    }






}
