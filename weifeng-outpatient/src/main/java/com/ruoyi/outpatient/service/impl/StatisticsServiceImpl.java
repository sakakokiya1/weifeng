package com.ruoyi.outpatient.service.impl;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.outpatient.core.SecurityUtils;
import com.ruoyi.outpatient.domain.Blog;
import com.ruoyi.outpatient.mapper.IBlogMapper;
import com.ruoyi.outpatient.service.IStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @author 伟峰
 * @date 2022/6/12
 * @description:
 */
@Service
public class StatisticsServiceImpl implements IStatisticsService {

    @Autowired
    private SecurityUtils securityUtils;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private IBlogMapper blogMapper;

    /**
     * 统计个人数据 新增粉丝 主页访客 观看数 互动数
     * @return
     */
    @Override
    public AjaxResult personDateSum() {

        Long loginPersonId = securityUtils.getLoginPersonId();

//        1.近30天新增粉丝
        Object cacheObject = redisCache.getCacheObject("statistics:myFansSum:" + loginPersonId);
        Long myFansSum=0L;
        if (StringUtils.isNotNull(cacheObject)){
            myFansSum=Long.valueOf(cacheObject.toString());
        }
//        2.主页访客总数
        Long personSum = redisCache.getCacheHyperLogSize("statistics:personPageView:"+loginPersonId);
//        3.观看blog总数
        Long blogSum = redisCache.getCacheHyperLogSize("statistics:allBlogPageView:"+loginPersonId);

//        4.互动数(点赞,收藏,评论总和)
        Long interactionSum = redisCache.getCacheHyperLogSize("statistics:interaction:"+loginPersonId);

        HashMap<String,Long> map=new HashMap<>(4);
        map.put("myFansSum",myFansSum);
        map.put("personSum",personSum);
        map.put("blogSum",blogSum);
        map.put("interactionSum",interactionSum);
        return AjaxResult.success().put("statistics",map);
    }

    /**
     * 统计当前笔记收藏点赞
     * @return
     */
    @Override
    public AjaxResult bloglikecollectAllSum() {
        Long loginPersonId = securityUtils.getLoginPersonId();
//        1.笔记
       long blogSum=blogMapper.selectCountBlog(loginPersonId);

//        2.收藏
        long collectsSum = blogMapper.selectBlogCollects(loginPersonId);
        List<Long> list=new ArrayList<>();
        list.add(blogSum+collectsSum);
        list.add(blogSum);
        list.add(collectsSum);
        return AjaxResult.success().put("list",list);
    }
}
