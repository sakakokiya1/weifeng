package com.ruoyi.outpatient.service;

import com.ruoyi.common.core.domain.AjaxResult;

/**
 * @author 伟峰
 * @date 2022/5/29
 * @description:
 */
public interface ISignService {
    /**
     * 用户进行每日签到
     * @return
     */
    Boolean setSign();

    /**
     * 用户每个月连续签到天数
     * @return
     */
    int getContinuous();

    /**
     * 显示签到天数
     * @return
     */
    AjaxResult showSign();
}
