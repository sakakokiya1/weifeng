package com.ruoyi.outpatient.service.impl;


import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.outpatient.domain.Patient;
import com.ruoyi.outpatient.service.IOptRegistrationService;
import com.ruoyi.system.domain.KsDept;
import com.ruoyi.system.mapper.SysKsDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OptRegistrantionServiceImpl implements IOptRegistrationService {

    @Autowired
    private SysKsDeptMapper ksDeptMapper;


    @Autowired
    private RedisCache redisCache;


    @Override
    public List<Patient> selectOptRegistrationList() {

        return null;
    }

    /**
     * 获取科室列表
     * @param ksDept
     * @return
     */
    @Override
    public AjaxResult selectKsDeptList(KsDept ksDept) {

//        先从redis中找
        Map<String, Object> cacheMap = redisCache.getCacheMap(Constants.KS_LIST);

        if (StringUtils.isNotNull(ksDept)){
            return AjaxResult.success().put(Constants.KS_LIST,cacheMap);
        }


//        1.先查询出所有的科室列表
        List<KsDept> list = ksDeptMapper.selectList(ksDept);

//        2.进行strea条件分类找出父级科室,1为父级
        List<KsDept> parent = list.stream().filter(
//                从list中将元素条件遍历，得到子集合
                ls -> {
                    return ls.getDeptSize() == 1;
                }).collect(Collectors.toList());//条件排除完在变成List集合

//        继续从集合中找出子级科室
        List<KsDept> child = list.stream().filter(
                son -> {
                    return son.getDeptSize() == 0;
                })
                .collect(Collectors.toList());

//        科室变换较少,存入redis,方便患者查询速度
//        先存map再存redisMap
        HashMap<String,List<KsDept>> map=new HashMap<>();
        map.put("parent",parent);
        map.put("child",child);

        redisCache.setCacheMap(Constants.KS_LIST,map);
        return AjaxResult.success().put(Constants.KS_LIST,map);
    }
}
