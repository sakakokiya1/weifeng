<p align="center">
	<img alt="logo" src="https://oscimg.oschina.net/oscnet/up-d3d0a9303e11d522a06cd263f3079027715.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">BlueBook v3.8.1</h1>
<h4 align="center">基于SpringBoot+Vue前后端分离的社交分享平台</h4>
<p align="center">
</p>

## 平台简介
一款社交类的软件，专注年轻人的笔记短视频社区，用户可以选择歌曲，配以短视频或图片，形成自己的作品，通过它你可以分享你的生活，同时也可以在这里认识到更多朋友，了解各种奇闻趣事。

**使用的技术栈**：SpringBoot + Mybatis + Vue + Redis + MySQL + RabbitMq + SpringSecurity


**代码**：自己实现的大部分代码位置ruoyi-admin模块，weifeng-outpatient模块、为了方便回忆所以代码里的**注释比较多**
前端地址：https://gitee.com/steamqaqwq/bluebook.git


## 一部分演示图
[![XHy9HO.png](https://s1.ax1x.com/2022/06/16/XHy9HO.png)](https://imgtu.com/i/XHy9HO)

[![XHs7HU.png](https://s1.ax1x.com/2022/06/16/XHs7HU.png)](https://imgtu.com/i/XHs7HU)

[![XHsUtH.png](https://s1.ax1x.com/2022/06/16/XHsUtH.png)](https://imgtu.com/i/XHsUtH)

[![XHySu6.png](https://s1.ax1x.com/2022/06/16/XHySu6.png)](https://imgtu.com/i/XHySu6)



## 一部分流程图 
1.详情页  2.笔记推送   3.点赞、收藏、关注
![gTi4F.png](https://s1.328888.xyz/2022/10/09/gTi4F.png)
