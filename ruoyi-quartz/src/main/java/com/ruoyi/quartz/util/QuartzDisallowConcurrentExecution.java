package com.ruoyi.quartz.util;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import com.ruoyi.quartz.domain.SysJob;
import org.quartz.PersistJobDataAfterExecution;

/**
 * 定时任务处理（禁止并发执行）
 * 
 * @author ruoyi
 *
 * 调度器每次执行时,都会根据JobDetil来创建新的Job对象，防止并发访问时使用同一个对象
 */
//@PersistJobDataAfterExecution //表示：持久化jobdetail,如果一个任务不是持久化,那么没有触发器关联他时,Quratz会将他删除
@DisallowConcurrentExecution //加上该注解就不会并发地执行任务,一个一个同步地执行任务
public class QuartzDisallowConcurrentExecution extends AbstractQuartzJob
{
    @Override
    protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception
    {
        JobInvokeUtil.invokeMethod(sysJob);
    }
}
