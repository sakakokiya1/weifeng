package com.ruoyi.quartz.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * 定时任务配置（单机部署建议删除此类和qrtz数据库表，默认走内存会最高效）
 * 
 * @author ruoyi
 */
@Configuration
public class ScheduleConfig
{

//    SchedulerFactoryBean注入到spring就会变成 Scheduler

//    配置读取quartz配置文件的Bean ，当他注入时将创建 Scheduler
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(DataSource dataSource)
    {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setDataSource(dataSource);

        // quartz参数
        Properties prop = new Properties();
                                //用来区分特定的调度器实例，可以按照功能用途来给调度器起名
        prop.put("org.quartz.scheduler.instanceName", "RuoyiScheduler");
                                //如果你想Quartz帮你生成这个值的话，可以设置为AUTO
        prop.put("org.quartz.scheduler.instanceId", "AUTO");

        // 线程池配置            //默认使用自带线程池
        prop.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
                                //线程数
        prop.put("org.quartz.threadPool.threadCount", "20");
                                //线程优先级
        prop.put("org.quartz.threadPool.threadPriority", "5");

        // JobStore配置          //即数据的持久化方式
        prop.put("org.quartz.jobStore.class", "org.springframework.scheduling.quartz.LocalDataSourceJobStore");
        // 集群配置               //是否支持集群
        prop.put("org.quartz.jobStore.isClustered", "true");
//                               //每个X秒检测集群是否在线
        prop.put("org.quartz.jobStore.clusterCheckinInterval", "15000");
        prop.put("org.quartz.jobStore.maxMisfiresToHandleAtATime", "1");
        prop.put("org.quartz.jobStore.txIsolationLevelSerializable", "true");

        // sqlserver 启用
        // prop.put("org.quartz.jobStore.selectWithLockSQL", "SELECT * FROM {0}LOCKS UPDLOCK WHERE LOCK_NAME = ?");
        prop.put("org.quartz.jobStore.misfireThreshold", "12000");
                                  //quartz表的前缀
        prop.put("org.quartz.jobStore.tablePrefix", "QRTZ_");
        factory.setQuartzProperties(prop); //将这些配置放入（建议使用配置文件形式）

        factory.setSchedulerName("RuoyiScheduler"); //调度器名字

        factory.setStartupDelay(1); //延迟执行的时间
        factory.setApplicationContextSchedulerContextKey("applicationContextKey"); //上下文名
        // 可选，QuartzScheduler
        // 启动时更新己存在的Job，这样就不用每次修改targetObject后删除qrtz_job_details表对应记录了
        factory.setOverwriteExistingJobs(true);
        // 设置自动启动，默认为true
        factory.setAutoStartup(true);

        return factory;
    }
}
