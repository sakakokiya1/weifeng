package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysEiSet;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysEiSetMapper {


    /**
     * 查询 检查项目设置 列表
     * @param sysEiSet
     * @return
     */
    List<SysEiSet> selectEiSetList(SysEiSet sysEiSet);

    /**
     * 查询检查项目实体类
     * @param eisetId 检查项目ID
     * @return 检查项目信息
     */
    SysEiSet selectEiSetById(Long eisetId);


    /**
     * 添加检查项目
     * @param sysEiSet 项目信息
     * @return int
     */
    int updateEiSetAdd(SysEiSet sysEiSet);

    /**
     * 删除 检查项目通过ID
     * @param eisetId 检查项目ID
     * @return int
     */
    int deleteEiSetById(Long eisetId);

    /**
     *  修改 检查项目信息
     * @param sysEiSet
     * @return
     */
    int ediEiSet(SysEiSet sysEiSet);
}
