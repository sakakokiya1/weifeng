package com.ruoyi.system.mapper;


import com.ruoyi.system.domain.KsDept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 科室表 数据层
 *
 * @author 伟峰
 */
@Mapper
public interface SysKsDeptMapper {

    /**
     * 查询科室列表
     * @param ksdept
     * @return
     */
    List<KsDept> selectList(KsDept ksdept);

    /**
     * 新增科室
     * @param ksdept
     * @return
     */
    int insert(KsDept ksdept);

    /**
     * 删除科室
     * @param ksdeptIds
     * @return
     */
    int deleteUserByIds(Long[] ksdeptIds);

    /**
     * 根据科室名查询科室信息
     * @param deptName 科室名
     * @return
     */
    KsDept checkKsDeptNameUnique(String deptName);

    /**
     * 更新科室信息
     * @param ksdept
     * @return
     */
    int updateKsDept(KsDept ksdept);

}
