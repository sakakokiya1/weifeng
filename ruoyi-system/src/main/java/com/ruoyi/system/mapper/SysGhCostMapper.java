package com.ruoyi.system.mapper;


import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.SysGhCost;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 挂号费用DAO
 */
@Mapper
public interface SysGhCostMapper {


    /**
     * 查询挂号费用列表集合
     * @param sysEiSet
     * @return
     */
    List<SysGhCost> selectGhCostList(SysGhCost sysEiSet);


    /**
     *  通过id查询挂号费用信息
     * @param ghcostId
     * @return 挂号费用信息
     */
    SysGhCost selectGhCostById(Long ghcostId);

    /**
     * 通过id删除挂号费用信息
     * @param ghcostId
     * @return
     */
    int deleteGhCostById(Long ghcostId);


    int editGhCostById(SysGhCost sysGhCost);

    /**
     * 通过name查询id是否唯一
     * @param ghcostName
     * @return
     */
    int selectGhCostName(String ghcostName);


   int addGhCost(SysGhCost sysGhCost);
}
