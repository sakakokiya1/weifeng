package com.ruoyi.system.service;


import com.ruoyi.system.domain.SysEiSet;
import org.springframework.stereotype.Service;

import java.util.List;



public interface ISysEiSetService {

    /**
     * 查询 检查项目设置 列表
     * @param sysEiSet
     * @return
     */
    List<SysEiSet> selectEiSetList(SysEiSet sysEiSet);

    /**
     * 添加 检查项目
     * @param sysEiSet 项目信息
     * @return int
     */
    int updateEiSetAdd(SysEiSet sysEiSet);

    /**
     * 删除 检查项目
     * @param eisetId
     * @return int
     */
    int deleteEiSetById(Long eisetId);


    /**
     *  修改 检查项目
     * @param sysEiSet 检查项目信息
     * @return int
     */
    int editEiSet(SysEiSet sysEiSet);
}
