package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.KsDept;
import com.ruoyi.system.mapper.SysKsDeptMapper;
import com.ruoyi.system.service.ISysKsDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 *  科室 业务层处理
 */
@Service
public class SysKsDeptServiceImpl implements ISysKsDeptService {

    @Autowired
    private SysKsDeptMapper sysKsDeptMapper;

    /**
     * 查询科室
     * @param ksdept
     * @return
     */
    @Override
    public List<KsDept> selectList(KsDept ksdept) {

        List<KsDept> list= sysKsDeptMapper.selectList(ksdept);
        return list;
    }

    @Override
    public int add(KsDept ksdept) {
        ksdept.setCreateTime(new Date());
        int rows=sysKsDeptMapper.insert(ksdept);
        return rows;
    }

    /**
     * 删除科室
     * @param ksdeptIds
     * @return
     */
    @Override
    public int deleteKsDeptByIds(Long[] ksdeptIds) {




        return sysKsDeptMapper.deleteUserByIds(ksdeptIds);
    }

    /**
     * 检查科室名字是否唯一
     * @param ksdept 科室信息
     * @return 字符串
     */
    @Override
    public String checkKsDeptNameUnique(KsDept ksdept) {
//        1.先判断科室名是否空
        Long i=StringUtils.isEmpty(ksdept.getDeptName()) ? -1L : ksdept.getDeptId();
//        2.查询科室名，是否唯一
        KsDept ksDepts=sysKsDeptMapper.checkKsDeptNameUnique(ksdept.getDeptName());
//        3.如果对象不为空 且 id不同
        if (StringUtils.isNotNull(ksDepts) && !ksDepts.getDeptId().equals(ksdept.getDeptId()))
        {
            return UserConstants.UNIQUE;
        }
        return UserConstants.NOT_UNIQUE;
    }

    /**
     * 修改科室
     * @param ksdept 科室信息
     * @return
     */
    @Override
    public int updateKsDept(KsDept ksdept) {
        return sysKsDeptMapper.updateKsDept(ksdept);
    }
}
