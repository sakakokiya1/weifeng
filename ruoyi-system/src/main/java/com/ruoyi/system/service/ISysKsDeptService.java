package com.ruoyi.system.service;


import com.ruoyi.system.domain.KsDept;

import java.util.List;

/**
 * 科室 业务层
 */
public interface ISysKsDeptService {


    /**
     *  根据条件 查询科室集合
     * @param ksdept
     * @return
     */
    List<KsDept> selectList(KsDept ksdept);

    /**
     * 新增科室
     * @return
     */
    int add(KsDept ksdept);

    /**
     * 删除科室
     * @param ksdeptIds
     * @return
     */
    int deleteKsDeptByIds(Long[] ksdeptIds);

    /**
     * 检查科室名字是否唯一
     * @param ksdept 科室信息
     * @return 字符串
     */
    String checkKsDeptNameUnique(KsDept ksdept);

    /**
     * 修改科室
     * @param ksdept 科室信息
     * @return
     */
    int updateKsDept(KsDept ksdept);
}
