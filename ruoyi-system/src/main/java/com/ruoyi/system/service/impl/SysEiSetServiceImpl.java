package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysEiSet;
import com.ruoyi.system.mapper.SysEiSetMapper;
import com.ruoyi.system.service.ISysEiSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysEiSetServiceImpl implements ISysEiSetService {

    @Autowired
    private SysEiSetMapper eiSetMapper;

    /**
     * 查询 检查项目设置 列表
     * @param sysEiSet
     * @return
     */
    @Override
    public List<SysEiSet> selectEiSetList(SysEiSet sysEiSet)  {

        return  eiSetMapper.selectEiSetList(sysEiSet);



    }

    /**
     * 添加检查项目
     * @param sysEiSet 项目信息
     * @return
     */
    @Override
    public int updateEiSetAdd(SysEiSet sysEiSet) {
        SysEiSet eiSet=eiSetMapper.selectEiSetById(sysEiSet.getEisetId());
        if (StringUtils.isNull(eiSet))
        {
            eiSet.setCreateTime(DateUtils.getNowDate());
            int rows=eiSetMapper.updateEiSetAdd(sysEiSet);
            return rows;
        }
        return 0;
    }

    /**
     * 删除 检查项目
     * @param eisetId 检查项目ID
     * @return int
     */
    @Override
    public int deleteEiSetById(Long eisetId) {
        return eiSetMapper.deleteEiSetById(eisetId);
    }

    /**
     *  修改 检查项目信息
     * @param sysEiSet 检查项目信息
     * @return int
     */
    @Override
    public int editEiSet(SysEiSet sysEiSet) {

        return eiSetMapper.ediEiSet(sysEiSet);

    }
}
