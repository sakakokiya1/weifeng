package com.ruoyi.system.service;


import com.ruoyi.system.domain.SysGhCost;

import java.util.List;

public interface ISysGhCostService {

    /**
     *  查询挂号费用列表
     * @param sysEiSet
     * @return
     */
    List<SysGhCost> selectGhCostList(SysGhCost sysEiSet);

    /**
     * 通过id删除挂号费用id
     * @param ghcostId
     * @return
     */
   int deleteGhCostById(Long ghcostId);
}
