package com.ruoyi.system.service.impl;


import com.ruoyi.system.domain.SysGhCost;
import com.ruoyi.system.mapper.SysGhCostMapper;
import com.ruoyi.system.service.ISysGhCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysGhCostServiceImpl implements ISysGhCostService {

    @Autowired
    private SysGhCostMapper sysGhCostMapper;


    /**
     * 挂号信息列表查询
     * @param sysEiSet 挂号信息
     * @return 列表集合
     */
    @Override
    public List<SysGhCost> selectGhCostList(SysGhCost sysEiSet) {

     return sysGhCostMapper.selectGhCostList(sysEiSet);


    }

    /**
     * 通过id删除挂号费用信息
     * @param ghcostId
     * @return
     */
    @Override
    public int deleteGhCostById(Long ghcostId) {

       return sysGhCostMapper.deleteGhCostById(ghcostId);

    }
}
