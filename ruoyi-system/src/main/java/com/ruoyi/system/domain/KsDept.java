package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Objects;

/**
 * 科室实体类型
 */
@ApiModel(value = "KsDept")
public class KsDept extends BaseEntity {


    @ApiModelProperty(value = "主键",position = 1)
    private Long deptId;

    @ApiModelProperty(value = "科室名",position = 2)
    private String deptName;

    @ApiModelProperty(value = "科室编码",position = 3)
    private String deptCode;

    @ApiModelProperty(value = "当前挂号量",position = 4)
    private Long deptNum;

    @ApiModelProperty(value = "负责人",position = 5)
    private String deptLeader;

    @ApiModelProperty(value = "手机号码",position = 6)
    private String deptPhone;

    @ApiModelProperty(value = "状态",position = 7)
    private String status;


    private Character deptSize;


    public KsDept() {
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    @NotBlank(message = "科室名称不能为空")
    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public Long getDeptNum() {
        return deptNum;
    }

    public void setDeptNum(Long deptNum) {
        this.deptNum = deptNum;
    }

    public String getDeptLeader() {
        return deptLeader;
    }

    public void setDeptLeader(String deptLeader) {
        this.deptLeader = deptLeader;
    }

    @Size(min = 1,max = 13,message = "电话号码")
    public String getDeptPhone() {
        return deptPhone;
    }

    public void setDeptPhone(String deptPhone) {
        this.deptPhone = deptPhone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Character getDeptSize() {
        return deptSize;
    }

    public void setDeptSize(Character deptSize) {
        this.deptSize = deptSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o == null || getClass() != o.getClass())
        {return false;}
        KsDept ksDept = (KsDept) o;
        return Objects.equals(deptId, ksDept.deptId) &&
                Objects.equals(deptName, ksDept.deptName) &&
                Objects.equals(deptCode, ksDept.deptCode) &&
                Objects.equals(deptNum, ksDept.deptNum) &&
                Objects.equals(deptLeader, ksDept.deptLeader) &&
                Objects.equals(deptPhone, ksDept.deptPhone) &&
                Objects.equals(deptSize, ksDept.deptSize) &&
                Objects.equals(status, ksDept.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deptSize,deptId, deptName, deptCode, deptNum, deptLeader, deptPhone, status);
    }

    @Override
    public String toString() {
        return "KsDept{" +
                "deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", deptCode='" + deptCode + '\'' +
                ", deptNum=" + deptNum +
                ", deptLeader='" + deptLeader + '\'' +
                ", deptPhone='" + deptPhone + '\'' +
                ", status='" + status + '\'' +
                ", deptSize='" + deptSize + '\'' +
                '}';
    }
}
