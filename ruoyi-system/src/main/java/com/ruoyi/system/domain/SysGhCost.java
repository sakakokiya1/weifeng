package com.ruoyi.system.domain;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class SysGhCost extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 挂号设置ID */
    private Long ghcostId;

    /** 挂号费用名称 */
    private String ghcostName;


    /** 挂号费 */
    private Double ghcostPrice;



    /** 状态 */
    private Character status;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getGhcostId() {
        return ghcostId;
    }

    public void setGhcostId(Long ghcostId) {
        this.ghcostId = ghcostId;
    }

    @NotBlank(message = "挂号费用名称不能为空")
    @Size(min = 0, max = 100, message = "挂号费用名称不能超过100个字符")
    public String getGhcostName() {
        return ghcostName;
    }

    public void setGhcostName(String ghcostName) {
        this.ghcostName = ghcostName;
    }




    public Double getGhcostPrice() {
        return ghcostPrice;
    }

    public void setGhcostPrice(Double ghcostPrice) {
        this.ghcostPrice = ghcostPrice;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SysGhCost{" +
                "ghcostId=" + ghcostId +
                ", ghcostName='" + ghcostName + '\'' +
                ", ghcostPrice=" + ghcostPrice +
                ", status=" + status +
                '}';
    }
}
