package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.ColumnType;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 检查项目配置表 sys_eiset
 * 
 * @author weifen
 */
public class SysEiSet extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 检查项目ID */
    @Excel(name = "检查项目ID", cellType = ColumnType.NUMERIC)
    private Long eisetId;

    /** 检查项目名称 */
    @Excel(name = "检查项目名称")
    private String eisetName;

    /** 关键字 */
    @Excel(name = "关键字")
    private String eisetKey;

    /** 项目单价 */
    @Excel(name = "项目单价")
    private Double eisetPrice;

    /** 项目成本 */
    @Excel(name = "项目成本")
    private Double eisetCost;

    /**单位*/
    @Excel(name = "单位")
    private String eisetUnit;


    /**类型*/
    @Excel(name = "类型")
    private String eisetType;

    /**状态*/
    @Excel(name = "状态")
    private Character status;



    public Long getEisetId() {
        return eisetId;
    }

    public void setEisetId(Long eisetId) {
        this.eisetId = eisetId;
    }

    @NotBlank(message = "参数名称不能为空")
    @Size(min = 0, max = 100, message = "参数名称不能超过100个字符")
    public String getEisetName() {
        return eisetName;
    }

    public void setEisetName(String eisetName) {
        this.eisetName = eisetName;
    }


    public String getEisetKey() {
        return eisetKey;
    }

    public void setEisetKey(String eisetKey) {
        this.eisetKey = eisetKey;
    }

    @NotBlank(message = "价格不能为空")
    @Size(min = 0,message = "价格最低位0")
    public Double getEisetPrice() {
        return eisetPrice;
    }

    public void setEisetPrice(Double eisetPrice) {
        this.eisetPrice = eisetPrice;
    }

    @NotBlank(message = "成本不能为空")
    @Size(min = 0,message = "成本最低位0")
    public Double getEisetCost() {
        return eisetCost;
    }

    public void setEisetCost(Double eisetCost) {
        this.eisetCost = eisetCost;
    }

    public String getEisetUnit() {
        return eisetUnit;
    }

    public void setEisetUnit(String eisetUnit) {
        this.eisetUnit = eisetUnit;
    }

    @NotBlank(message = "类型不能为空")
    public String getEisetType() {
        return eisetType;
    }

    public void setEisetType(String eisetType) {
        this.eisetType = eisetType;
    }


    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SysEiSet{" +
                "eisetId=" + eisetId +
                ", eisetName='" + eisetName + '\'' +
                ", eisetKey='" + eisetKey + '\'' +
                ", eisetPrice=" + eisetPrice +
                ", eisetCost=" + eisetCost +
                ", eisetUnit='" + eisetUnit + '\'' +
                ", eisetType='" + eisetType + '\'' +
                ", status=" + status +
                '}';
    }
}
