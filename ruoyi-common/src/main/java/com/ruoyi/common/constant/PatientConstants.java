package com.ruoyi.common.constant;


/**
 * 患者常量
 */
public class PatientConstants {

    /**
     * 对象不为空
     */
    public static final Integer Object_IS_One=1;

    /**
     * 患者存储token的redis键
     */
    public static final String PATIENT_TOKEN_KEY="patient_token_key:";

    /**
     *  token超时时间
     */
    public static final Integer Expire_Time=600;


}
