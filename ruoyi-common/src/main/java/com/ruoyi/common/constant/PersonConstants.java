package com.ruoyi.common.constant;
/**
 * Demo class
 *
 * @author 伟峰
 * @date 2022/4/15
 */
public class PersonConstants {

    /**
     * redis存储短信验证码 5分钟
     */
    public static final String LOGIN_PHONE="login:phone:";

    /**
     * redis存储验证码过期时间
     */
    public static final String LOGIN_VERIFY="login:verify:";

    /**
     * token字符集
     */
    public static final String LOGIN_TOKEN_KEY = "login:tokens:";


    /**
     * blog文件或图片上传
     */
    public static final String BLOG_UPLOAD = "blog:upload:";

    /**
     * tag最新时间
     */
    public static final String TAG_NEWTIME = "tag:newtime:";


    /**
     * 一个tagName存储多个blogid+时间戳
     */
    public static final String TAG_BLOGID = "tag:blogid:";

    /**
     * 通过单个tagName检索多个（篮：{篮球 篮球火}+时间戳）
     */
    public static final String TAG_TAGNAMES = "tag:tagNames:";

    /**
     * 上传分享详情信息
     */
    public static final String BLOG_DETAILS = "blog:details:";

    /**
     * 个人资料
     */
    public static final String PERSON_INFORMATION = "person:information:";

    /**
     * 验证码有效期为60秒
     */
    public static final Integer VERIFY_INDATE= 60000;

    /**
     * 首次登录性别设置 默认'男'
     */
    public static final Character SEX= '男';



    public static final String RECOMMEND_TAGSCORE= "recommend:tagscore:";


    /**
     *
     */
    public static final String FOLLOW_FANS= "follow:fans:";


    /**
     * 查询10公里内的人
     */
    public static final int SCOPE= 10;


    /**
     * 图片0还是视频1
     */
    public static final Character isVideo='1';






}
