package com.ruoyi.common.utils;

import com.github.pagehelper.PageHelper;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.utils.sql.SqlUtil;

/**
 * 分页工具类
 * 
 * @author ruoyi
 */
public class PageUtils extends PageHelper
{
    /**
     * 设置请求分页数据
     */
    public static void startPage()
    {
//        获取前端分页参数等
        PageDomain pageDomain = TableSupport.buildPageRequest();

//        放入当前页+记录数
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();

        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize))
        {
//            检查字符，防止注入绕过
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
//            看前端参数reasonable是否合理化
//            在启用合理化时，如果 pageNum<1，则会查询第一页；如果 pageNum>pages，则会查询最后一页。
            Boolean reasonable = pageDomain.getReasonable();

//            mybatis插件 PageHelper进行分页
            PageHelper.startPage(pageNum, pageSize, orderBy).setReasonable(reasonable);
        }
    }
}
