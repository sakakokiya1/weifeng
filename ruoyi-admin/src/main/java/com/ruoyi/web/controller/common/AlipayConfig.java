package com.ruoyi.web.controller.common;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.ruoyi.outpatient.domain.PayEnpity;
import org.springframework.context.annotation.Configuration;


/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

@Configuration
public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2021000119690838";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key="MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDRTcnPbztubwtlLuq6IKbZi5JLiQHsVtGWUA2sNu2OxbyRi2qGe+go8oVDqOp4Xr14xMzjm67tyifVolJnYAyAhzjr8FFMxAFeyZtXTfkR9ePoeXywCt/fHfP3CiS2cxnFFMmAy2QH8vBee7Ro9iOY6TEXnPJuWTdPQpT7zqXLiRUkq36UBBDDR4Y8Rmu7tMEn5tQajQSlL6bx27UP79HP6gahr+tSXE65Z8J1hH0lWJvFfhYZpFMOFv7V2edHa9+TxUJTb7rgfEIXOG179t2BkwqWBBjVYMyKA1kr4QTomgegYcCSUskQrqKpvP5o/IsUhVM9SQzZpo/n9ckPRjNXAgMBAAECggEBAI9LNlMxz/p3Q9HuIP10xUZC69GS5yIq/ipufC3fHRyV+9eZH0EbK4J9Jwd/aacnFYjNE4jwWP6rXEJCczAa7fRAn5PIX6CywE1osVVAKLlHItVR9y9/Am36T8BdzWyruITZCmuG0+EtTash/g2uFgjl/Zow9TjfE6LL7rvGanPBFSh+VsYdDUDBJLEcXojhDeU4SR02vqSfflQMYvj9YfQ8bmI/Dq/E5NBUIS2ySrVy4/T5d3KKLjozBqSRO5NlPYfL5zJWhbfRzyhi2I2XELb3BTAevtD2Iw8Wx94hpJQuhTt1mlxqoFMKvJS+EGZL9tHjlfDR3AvwCrVQcsYvB+ECgYEA57fGxHp85LXjgSP6kIZTSa/6zsTNUZol/Y71WRoYLA7cRdJUmPmyNYOtzvV3chPE3u1QAuWzMmvm5OY8vkvxIB646quDsmTzm+71hwjgNnXB99reczVGzbT+ay1LitqjRtQolqc2dEQ+k6SCQV2ISPdgXwm3oj/tZoxkpIZz9OMCgYEA5zy30QLMPJDEI9jGdfX1l3m2ZcBgWQjnLs8tV8DNBKnl6bcoGkc1JxQ3sc5+sm1zjMfx73Cj9dD9XLGyebmKZhno56FctR8VIJw73ZGyFVvJyrsf5tqRPi4tIERpgVMTV3XqTTaaFDll0ELzADWVF+1izcnyVrBisd/IU7dkRf0CgYAyqMMUAZN8jSeH/E2zeyqKWacDvZMF3CWiIbcSG9pdB4Kw4SMNrsl/WaKONKxAjIHmGwv7FizvQRfhw1RUZlX5J75Vz9mjDt8ToFKdixs5zNTzUy5/EY7BOmjFFcoGOXao7YlSJoyJLsJflP/FOqWpDixLmtxC5KcKXHdEYQDI8QKBgQDZFqjfwceqaKKroL0ZS5OI5Qx3VHdfvzsDOB+yjeAsK7EcjQZKPWXKTXHILTvHGXhsx9370/0qL01ds7oCC03Cczq0E0qtBARavDJ73meZI7Y77ReTLMlE3ichbvi6IBs9dgdE/CkFKanLLr9iB+v0LbofxyMLfcb6hURWTAsIpQKBgQCfgZqWQgANPdpCbxTd+EDQD8CtBxqVOGXGkOrQQiyI3BHrEP3ZjacZkebMBZqSdfCtgVZQEG9+ZjkRpo6upPAqMbk9cFA7MoRnIRT+CbcLjB835rq+ZBGs/s4+GwRm+IVvfxKmspe9EpElfSLInfWlmnubFZzzg+xMu5AxvA05Ww==";

	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAi1/H1E/j77oGc+1paxPRoCaZnLx0ZBSPjuRQfh2Be89Jf7AeVgOFGXTE2/TCmhPuWR2fGMgwTXh8035fzANh+El5QFuWYReuXfmLYMW+CZFCKfy4IulmvviyqsXLG0CgzvGpOJqjc2UGduTfBFFfdK6no8nb3mCvwNfOwjzNhaeuaHJCQ3St7ejjH1i4b7T1pVBJJmLArI5c9bYag3FmpQqcgODm1G8cv7V5ejFLWEGNs6sfKdrbF/fziHkQstvuyIIxGEDWVc8MSgi7DyYyuSHrF9C45Ho8X7ASRNllIb8ahn9RGj+0nE7mTh9VGkqMG+P66u5ewMmzKoC6kbMBIQIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:8080/blog/list";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:8080/blog/list";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "C:\\";



	public String pay(PayEnpity payEnpity) throws AlipayApiException {

//	 1.   根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient=new DefaultAlipayClient(
                gatewayUrl,app_id,merchant_private_key,"json",
                charset,alipay_public_key,sign_type
        );


//        2.创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest payRequest=new AlipayTradePagePayRequest();
        payRequest.setReturnUrl(return_url);
        payRequest.setNotifyUrl(notify_url);


//        3.打赏订单号，唯一订单号  必填
        String out_trade_no = payEnpity.getOut_trade_no();
//        付款金额
        String total_amount = payEnpity.getTotal_amount();
//        订单名称
        String subject = payEnpity.getSubject();

//        String product_code = payEnpity.getProduct_code();
        String body = payEnpity.getBody();

        payRequest.setBizContent(
                "{\"out_trade_no\":\""+out_trade_no+"\","
                +"\"total_amount\":\""+total_amount+"\","
                +"\"subject\":\""+subject+"\","
                +"\"body\":\""+body+"\","
                +"\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}"
        );




        String result = alipayClient.pageExecute(payRequest).getBody();

//        会收到支付宝的响应,响应的是一个页面,只要浏览器显示这个页面,就会自动来到支付宝的收银台页面
        System.out.println("支付宝响应："+result);
        return result;
    }




//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
//    public static void logResult(String sWord) {
//        FileWriter writer = null;
//        try {
//            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
//            writer.write(sWord);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
}

