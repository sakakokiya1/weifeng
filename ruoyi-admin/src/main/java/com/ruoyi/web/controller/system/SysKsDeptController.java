package com.ruoyi.web.controller.system;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.KsDept;
import com.ruoyi.system.service.ISysKsDeptService;
import com.ruoyi.system.service.impl.SysKsDeptServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Api(value = "科室管理接口")
@RestController
@RequestMapping("/system/ksdept")
public class SysKsDeptController extends BaseController {


    @Autowired
    private ISysKsDeptService sysKsDeptService;




    @ApiOperation(value = "获取科室列表",notes = "查询数据库中某个学生")
    @PreAuthorize("@ss.hasAnyPermi('system:ksdept:query')")
    @GetMapping("/list")
    public TableDataInfo list(KsDept ksdept)
    {
//        获取前端分页参数
        startPage(); //该方法是父类中的

        List<KsDept> list=sysKsDeptService.selectList(ksdept);
        return getDataTable(list);
    }


    @ApiOperation("新增科室")
    @PreAuthorize("@ss.hasPermi('system:ksdept:add')")
    @PostMapping
    public AjaxResult addKsDept(@Validated @RequestBody KsDept ksDept)
    {

        ksDept.setCreateBy(SecurityUtils.getUsername());
        int rows=sysKsDeptService.add(ksDept);

        return toAjax(rows);

    }





    /**
     * 删除用户
     *
     */
    @PreAuthorize("@ss.hasPermi('system:ksdept:remove')")
    @DeleteMapping("/{ksdeptIds}")
    public AjaxResult remove(@PathVariable Long[] ksdeptIds)
    {

        return toAjax(sysKsDeptService.deleteKsDeptByIds(ksdeptIds));
    }



    /**
     * 修改部门
     */
    @PreAuthorize("@ss.hasPermi('system:ksdept:edit')")
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody KsDept ksdept)
    {

        if (UserConstants.NOT_UNIQUE.equals(sysKsDeptService.checkKsDeptNameUnique(ksdept)))
        {
            return AjaxResult.error("修改科室' "+ksdept.getDeptName()+" '失败，部门名称已存在");
        }


        ksdept.setUpdateBy(getUsername());
        return toAjax(sysKsDeptService.updateKsDept(ksdept));
    }



}
