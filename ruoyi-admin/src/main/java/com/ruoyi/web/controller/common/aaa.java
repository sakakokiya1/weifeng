package com.ruoyi.web.controller.common;



import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoResponse;

import static com.ruoyi.web.controller.common.Video.initVodClient;

/**
 * @author 伟峰
 * @date 2022/6/13
 * @description: 测试
 */
public class aaa {




    /**
     * 获取视频信息
     * @param client 发送请求客户端
     * @return GetVideoInfoResponse 获取视频信息响应数据
     * @throws Exception
     */
    public static GetVideoInfoResponse getVideoInfo(DefaultAcsClient client) throws Exception {
        GetVideoInfoRequest request = new GetVideoInfoRequest();
        request.setVideoId("fc43987f60e345bc866fa6348501b13d");
        return client.getAcsResponse(request);
    }

    // 请求示例
    public static void main(String[] argv) throws ClientException {
        DefaultAcsClient client = initVodClient("LTAI5t6gigdF6DhkXxTA6bb7", "unUJcpvIuTjkvINLlgdBdToZp6YfN2");
        GetVideoInfoResponse response = new GetVideoInfoResponse();
        try {
            response = getVideoInfo(client);
            System.out.print("Title = " + response.getVideo().getTitle() + "\n");
            System.out.print("Description = " + response.getVideo().getDescription() + "\n");
            System.out.println("URL"+response.getVideo().getCoverURL());

        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
        }
        System.out.print("RequestId = " + response.getRequestId() + "\n");
    }
}
