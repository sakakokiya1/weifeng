package com.ruoyi.web.controller.outpatient;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.domain.PayEnpity;
import com.ruoyi.outpatient.service.IPayService;
import com.ruoyi.web.controller.common.AlipayConfig;

import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 伟峰
 * @date 2022/5/11
 * @description: 支付controller
 */
@RestController
@RequestMapping("/pay")
public class PayController extends BaseController {


    @Autowired
    private AlipayConfig alipayConfig;

    @Autowired
    private IPayService payService;

    @GetMapping(value = "/order")
    public AjaxResult personPay( //订单号
            @RequestParam("orderNumber")String orderNumber
    ) throws AlipayApiException {



//        获取当前订单的支付信息
        PayEnpity orderPay = payService.getOrderPay(orderNumber);

        String pay = alipayConfig.pay(orderPay);




        System.out.println(pay);
        return AjaxResult.success().put("pay",pay);
    }
}
