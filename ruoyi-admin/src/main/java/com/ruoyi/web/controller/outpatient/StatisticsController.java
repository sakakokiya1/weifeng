package com.ruoyi.web.controller.outpatient;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.service.IStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 伟峰
 * @date 2022/6/12
 * @description: 统计个人数据
 */
@RestController
@RequestMapping("/statistics")
public class StatisticsController {


    @Autowired
    private IStatisticsService statisticsService;

    /**
     * 统计个人数据
     * @return
     */
    @GetMapping("/person")
    public AjaxResult personSum()
    {
        return statisticsService.personDateSum();

    }


    /**
     * 统计当前笔记收藏点赞
     * @return
     */
    @GetMapping("/bloglikecollect")
    public AjaxResult bloglikecollectAllSum()
    {
        return statisticsService.bloglikecollectAllSum();
    }

}
