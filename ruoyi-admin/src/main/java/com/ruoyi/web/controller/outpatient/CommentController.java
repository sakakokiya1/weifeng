package com.ruoyi.web.controller.outpatient;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.domain.Comment;
import com.ruoyi.outpatient.domain.CommentVo;
import com.ruoyi.outpatient.domain.Reply;
import com.ruoyi.outpatient.mapper.ICommentMapper;
import com.ruoyi.outpatient.service.ICommentService;
import com.ruoyi.outpatient.service.IReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 伟峰
 * @date 2022/5/19
 * @description: 评论controller
 */

@RestController
@RequestMapping("/comment")
public class CommentController extends BaseController {



    @Autowired
    private ICommentService commentService;

    @Autowired
    private IReplyService replyService;


    /**
     * 查询评论下的回复
     * @param blogId 分享id
     * @param commentId 评论id
     * @return 回复list
     */
    @GetMapping("/replyList/{blogId}/{commentId}")
    public AjaxResult replyList(
            @PathVariable("blogId")Long blogId,
            @PathVariable("commentId")Long commentId
    ){

        return null;



    }




    /**
     * 查询评论下的回复
     * @param blogId 分享id
     * @param createTime 根据评论时间查询
     * @return 回复list
     */
    @GetMapping("/commentList/{blogId}/{commentId}")
    public AjaxResult commentList(
            @PathVariable("blogId")Long blogId,
            @PathVariable("createTime")Long createTime
    ){


        AjaxResult commentList= commentService.selectCommentList(blogId,createTime);

        return commentList;
    }


    /**
     * 对blog进行评论
     * @param comment bolgID
     * @return
     */
    @PostMapping("/addComment")
    public AjaxResult addComment(
           Comment comment
    ){

        AjaxResult commentList= commentService.addComment(comment);

        return commentList;
    }


    /**
     * 对评论和回复进行回复
     * @param comment bolgID
     * @return
     */
    @PostMapping("/addReply")
    public AjaxResult addReply(
            Reply reply
    ){

        AjaxResult replyList= replyService.addReply(reply);

        return replyList;
    }




}
