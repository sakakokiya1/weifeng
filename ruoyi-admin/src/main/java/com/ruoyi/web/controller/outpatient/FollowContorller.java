package com.ruoyi.web.controller.outpatient;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.service.IFollowService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @author 伟峰
 * @date 2022/5/4
 * @description: 关注controller
 */
@RestController
@RequestMapping("/follow")
public class FollowContorller extends BaseController {

    @Autowired
    private IFollowService followService;




    /**
     * 判断是否关注过
     * @param followPersonId 用户id
     * @return
     */
    @GetMapping("/isfollow/{followPersonId}")
    public AjaxResult isfollow(
            @NotNull(message = "被关注id不能为空")
            @PathVariable("followPersonId")Long followPersonId
    ) {
        return followService.isfollow(followPersonId);
    }



    /**
     * 进行关注或取消关注
     * @param followPersonId 想要关注的用户id
     * @param bool true为关注，false为取关
     * @return
     */
    @GetMapping("/{followPersonId}/{bool}")
    public AjaxResult follow(
            @NotNull(message = "被关注id不能为空")
            @PathVariable("followPersonId")Long followPersonId,
            @NotNull(message = "bool不能为空")
            @PathVariable("bool")Boolean bool
    ) {
        return followService.updateFollowByPersonId(followPersonId,bool);
    }



    /**
     * 共同关注列表
     * @return
     */
    @GetMapping("/common/{id}")
    public AjaxResult commonList(
            @NotNull(message = "被关注id不能为空")
            @PathVariable("id")Long id
    ) {

        return followService.commonList(id);
    }


    /**
     * 消息推送到粉丝
     * @param lastTime 本次要查询开始时间,也是上次结束时间
     * @param offset 查询偏移量 如果默认0即第一次查询
     * @return
     */
    @GetMapping("/message")
    public AjaxResult messageList(
            @RequestParam("lastTime")Long lastTime,
            @RequestParam(value = "offset",defaultValue = "0")Long offset
            ) {

        return followService.messageList(lastTime,offset);
    }






    /**
     * 关注我的粉丝列表
     * @return
     */
    @GetMapping("/fans")
    public AjaxResult fansList() {

        return followService.fansList();
    }


    /**
     * 我关注的博主列表
     * @return
     */
    @GetMapping("/bloger")
    public AjaxResult blogerList() {

        return followService.blogerList();
    }


}
