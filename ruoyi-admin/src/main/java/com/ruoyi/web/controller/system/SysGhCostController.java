package com.ruoyi.web.controller.system;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysEiSet;
import com.ruoyi.system.domain.SysGhCost;
import com.ruoyi.system.mapper.SysGhCostMapper;
import com.ruoyi.system.service.ISysGhCostService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/system/ghcost")
public class SysGhCostController  extends BaseController {


    @Autowired
    private ISysGhCostService ghCostService;

    @Autowired
    private SysGhCostMapper ghCostMapper;


    /**
     *  获取挂号费用列表
     * @param sysGhCost 挂号信息
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:ghcost:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysGhCost sysGhCost)
    {
        startPage();
        List<SysGhCost> list = ghCostService.selectGhCostList(sysGhCost);
        return getDataTable(list);

    }


    @PreAuthorize("@ss.hasPermi('system:ghcost:delete')")
    @Delete("/delete/{ghcostId}")
    public AjaxResult delete(@RequestParam("ghcostId") Long ghcostId) {

        if (StringUtils.isNotNull(ghcostId) && StringUtils.isNotNull(ghCostMapper.selectGhCostById(ghcostId)))
        {

            return toAjax(ghCostService.deleteGhCostById(ghcostId));

        }

        return toAjax(false);
    }


    /**
     * 修改挂号费用信息
     */
    @PreAuthorize("@ss.hasPermi('system:ghcost:edit')")
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysGhCost sysGhCost)
    {
        return toAjax(ghCostMapper.editGhCostById(sysGhCost));
    }


    /**
     * 新增挂号费用信息
     */
    @PreAuthorize("@ss.hasPermi('system:ghcost:add')")
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysGhCost sysGhCost)
    {
        if (StringUtils.isNull(ghCostMapper.selectGhCostName(sysGhCost.getGhcostName())))
        {
            return AjaxResult.error("该挂号项目已存在");
        }
        sysGhCost.setCreateTime(DateUtils.getNowDate());

        return AjaxResult.success(ghCostMapper.addGhCost(sysGhCost));
    }


}
