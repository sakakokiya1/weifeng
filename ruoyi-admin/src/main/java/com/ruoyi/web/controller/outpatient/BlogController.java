package com.ruoyi.web.controller.outpatient;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.outpatient.domain.Blog;
import com.ruoyi.outpatient.service.IBlogService;
import com.ruoyi.system.domain.SysNotice;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author 伟峰
 * @date 2022/4/29
 * @description:
 */

@RestController
@RequestMapping("/blog")
public class BlogController extends BaseController {

    @Autowired
    private IBlogService blogService;

    /**
     * 通过分享id查询详情信息
     * @param blogId 分享id
     * @return 分享主题详情信息
     */
    @GetMapping("/{blogId}")
    public AjaxResult blogDetails(@PathVariable("blogId") Long blogId){


        startPage();
        AjaxResult detils=blogService.selectBlogDetails(blogId);

        return detils;
    }


    /**
     * 对blog进行点赞
     * @param blogId
     * @return
     */
    @GetMapping("/likes")
    public AjaxResult likes(
            @NotNull(message = "blogId不能为空")
            @RequestParam("blogId")Long blogId
            ){

        return  blogService.likeBlog(blogId);

    }

    /**
     * 对blog进行收藏
     * @param blogId
     * @return
     */
    @GetMapping("/collect")
    public AjaxResult collect(
            @NotNull(message = "blogId不能为空")
            @RequestParam("blogId")Long blogId
    ){

        return  blogService.likeCollect(blogId);

    }







    /**
     * 附近的人
     * @param longitude
     * @param latitude
     * @return
     */
    @GetMapping("/nearby")
    public AjaxResult nearbyBlog(
            @RequestParam("longitude")Double longitude,
                                 @RequestParam("latitude")Double latitude){

        startPage();
        List<Blog> blogList= blogService.selectNearbyBlog(longitude,latitude);
        return AjaxResult.success().put("list",blogList);

    }


    /**
     * 首页推荐（核心）
     * @return
     */
    @GetMapping("/blogRecommend")
    public AjaxResult blogRecommend(){

        startPage();
        List<Blog> blogList= blogService.selectBlogList();
        return AjaxResult.success().put("list",blogList);

    }




    /**
     * 个人发布笔记管理中心
     * @return
     */
    @GetMapping("/personBlog")
    public AjaxResult personBlog(){


        startPage();
        AjaxResult blogList= blogService.personBlogManagement();
        return AjaxResult.success().put("list",blogList);

    }







    // TODO: 2022/6/5 后台部分



    /**
     * 后台获取BLOG列表
     */
//    @PreAuthorize("@ss.hasPermi('system:blog:list')")
    @GetMapping("/list")
    public TableDataInfo GBlogList(Blog blog)
    {
        startPage();
        List<Blog> list=blogService.selectGBlogList(blog);
        return getDataTable(list);
    }


    /**
     * 后台根据id删除BLOG列表
     */
//    @PreAuthorize("@ss.hasPermi('system:blog:remove')")
//    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{blogIds}")
    public AjaxResult remove(@PathVariable Long[] blogIds)
    {
        return toAjax(blogService.deleteBlogByIds(blogIds));
    }


    /**
     * 后台插入blog信息
     */
//    @PreAuthorize("@ss.hasPermi('system:blog:add')")
//    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@Validated @RequestBody Blog blog)
    {
//        notice.setCreateBy(getUsername());
        return toAjax(blogService.insertGBlog(blog));
    }



    /**
     * 修改通知公告
     */
//    @PreAuthorize("@ss.hasPermi('system:blog:edit')")
//    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@Validated @RequestBody Blog blog)
    {
//        notice.setUpdateBy(getUsername());
        return toAjax(blogService.updateBlog(blog));
    }

}
