package com.ruoyi.web.controller.outpatient;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.outpatient.service.ICommentService;
import com.ruoyi.outpatient.service.IReplyService;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

/**
 * @author 伟峰
 * @date 2022/5/15
 * @description: 点赞实体类
 */
@RestController
@RequestMapping("/like")
public class LikeController extends BaseController {

    @Autowired
    private ICommentService commentService;


    @Autowired
    private IReplyService replyService;
    /**
     * 对该评论进行点赞
     * @param commentId 评论id
     * @return
     */
    @GetMapping("/comment")
    public AjaxResult commentLike(
            @RequestParam(value = "commentId",required = false)Long commentId,
            @RequestParam(value = "replyId",required = false)Long replyId
    )
    {

//        如果评论和回复一起都点赞 或都没有点赞 则是无效请求
        if (StringUtils.isNull(commentId) && StringUtils.isNull(replyId)){
            return AjaxResult.error("点赞无效");
        }
        if (StringUtils.isNotNull(commentId) && StringUtils.isNotNull(replyId)){
            return AjaxResult.error("点赞无效");
        }


//        说明点赞的是评论
        if (StringUtils.isNotNull(commentId)){
           return commentService.likeComment(commentId);
        }

//        说明点赞的是回复
        if (StringUtils.isNotNull(replyId)){
          return   replyService.likeReply(replyId);
        }

        return null;
    }


}
