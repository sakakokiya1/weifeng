package com.ruoyi.web.controller.outpatient;

import cn.hutool.core.date.DateUtil;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.outpatient.core.LoginPatient;
import com.ruoyi.outpatient.service.ISignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * @author 伟峰
 * @date 2022/5/15
 * @description: 签到controller
 */

@RestController
@RequestMapping("/sign")
public class SignController {

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ISignService signService;


    /**
     * 用户进行每日签到
     * @return
     */
    @GetMapping("/person")
    public AjaxResult sign()
    {
        Boolean aBoolean = signService.setSign();
        return AjaxResult.success().put("sign",aBoolean);
    }



    /**
     * 用户每个月连续签到天数
     * @return
     */
    @GetMapping("/continuous")
    public AjaxResult continuous()
    {
//        从最后一次签到开始向前统计，逐个变量,知道未签到，计算总签到数
        int sum = signService.getContinuous();
        return AjaxResult.success().put("sign",sum);
    }


    /**
     * 显示签到日数
     * @return
     */
    @GetMapping("/showSign")
    public AjaxResult showSign()
    {
//        从最后一次签到开始向前统计，逐个变量,知道未签到，计算总签到数
        return signService.showSign();

    }



}
