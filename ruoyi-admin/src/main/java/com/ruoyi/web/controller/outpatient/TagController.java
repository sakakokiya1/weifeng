package com.ruoyi.web.controller.outpatient;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.outpatient.domain.Blog;
import com.ruoyi.outpatient.domain.Tag;
import com.ruoyi.outpatient.mapper.ITagMapper;
import com.ruoyi.outpatient.service.IBlogService;
import com.ruoyi.outpatient.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 伟峰
 * @date 2022/4/22
 * @description: 标签controller
 */
@RestController
@RequestMapping("/tag")
public class TagController extends BaseController {


    @Autowired
    private RedisCache redisCache;


    @Autowired
    private ITagMapper tagMapper;

    @Autowired
    private ITagService tagService;


    /**
     * 通过标签搜索 综合排序
     * @param tagName
     * @return
     */
    @PostMapping("/comprehensive")
    public AjaxResult comprehensiveSerach(
            @NotNull(message = "标签不能为空")
            @RequestParam("tagName")String tagName){

        AjaxResult result=tagService.selectComprehenBlogList(tagName);
        return result;
    }

    /**
     * 搜索最新，为协同过滤使用
     * @param tag 标签名
     * @return zset投喂滚动或list
     *
     */
//    @PostMapping("/newest")
//    public AjaxResult tagNewest(@Validated @RequestBody Tag tag){
//
//        if (StringUtils.isNotNull(tag.getTagName())){
//            return tagService.selectNewTagList(tag);
//        }
//        return null;
//    }


    /**
     * 通过标签搜索最新
     * @param tagName
     * @return
     */
    @PostMapping("/newest")
    public AjaxResult tagNewest(@RequestParam("tagName")
                                            @NotNull(message = "标签名不能为空")
                                            String tagName){

        Tag tag=new Tag();
        tag.setTagName(tagName);
        if (StringUtils.isNotNull(tagName)){
            return tagService.selectNewTagList(tag);
        }
        return null;
    }

    /**
     * 通过tag检索最热门
     * @param tagName 由：tagId+tagName
     * @return list
     */
    @PostMapping("/hot")
    public AjaxResult tagHot(
            @NotNull(message = "标签不能为空")
            @RequestParam("tagName")String tagName){

        AjaxResult result=tagService.selectBlogListByTagName(tagName);
      return result;
    }


    // TODO: 2022/6/5 后台tag


    @Autowired
    private IBlogService blogService;
    /**
     * 后台获取BLOG列表
     */
//    @PreAuthorize("@ss.hasPermi('system:tag:list')")
    @GetMapping("/list")
    public TableDataInfo GBlogList(Tag tag)
    {
        startPage();
        List<Tag> list=tagService.selectGTagList(tag);
        return getDataTable(list);
    }


    /**
     * 后台根据id删除BLOG列表
     */
//    @PreAuthorize("@ss.hasPermi('system:blog:remove')")
//    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{tagIds}")
    public AjaxResult remove(@PathVariable Long[] tagIds)
    {
        return toAjax(tagService.deleteGTagByIds(tagIds));
    }


    /**
     * 后台插入blog信息
     */
//    @PreAuthorize("@ss.hasPermi('system:blog:add')")
//    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@Validated @RequestBody Tag tag)
    {
//        notice.setCreateBy(getUsername());
        return toAjax(tagService.insertGTag(tag));
    }



    /**
     * 修改通知公告
     */
//    @PreAuthorize("@ss.hasPermi('system:blog:edit')")
//    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@Validated @RequestBody Tag tag)
    {
//        notice.setUpdateBy(getUsername());
        return toAjax(tagService.updateGTag(tag));
    }

}
