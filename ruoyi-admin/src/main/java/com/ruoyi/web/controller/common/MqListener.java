package com.ruoyi.web.controller.common;

import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.outpatient.common.MqConstants;
import com.ruoyi.outpatient.core.RedisBloomFilter;
import com.ruoyi.outpatient.mapper.IFollowMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author 伟峰
 * @date 2022/6/4
 * @description:
 */
@Component
public class MqListener {
    @Autowired
    private IFollowMapper followMapper;

    @Autowired
    private RedisBloomFilter bloomFilter;

    @Autowired
    private RedisCache redisCache;

    /**
     * 监听新增的服务处理
     * @param map 监听推介列表需要进行布隆过滤的ID和用户ID
     */
    @RabbitListener(queues = MqConstants.INSERT_QUEUE) //insert队列
    public void listenerInsertService(HashMap<String, List<Long>> map)
    {

//        获取map对应的key,也就是用户ID
        String keys="";
        Set<String> keySet = map.keySet();
        // 遍历keySet，并输出key的值
        for(String key : keySet){
            keys=key;
        }

//        将看过的笔记id插入布隆过滤器,防止用户下一次刷到
//        后期优化为管道批量插入
        for (Long l:map.get(keys))
        {
            bloomFilter.insert(keys,l);
        }

    }


    //
    /**
     * 监听上传blog推送到粉丝
     * @param map 监听推介列表需要进行布隆过滤的ID和用户ID
     */
    @RabbitListener(queues = MqConstants.INSERT_BLOGTOFANS_QUEUE) //insert队列
    public void listenerInsertBLOGTOFANSService(Long loginPersonId)
    {

//      分享文章存储完后,推送给关注自己的那些粉丝
//        1.查询作者的所有粉丝
        List<Long> list=followMapper.selectMyFansList(loginPersonId);


//        2.推送笔记给所有粉丝,用zset记录下来
        for (Long fans:list){

            String key="follow:feed:"+loginPersonId;
            redisCache.setCacheZSet(key,fans,System.currentTimeMillis());

        }

    }

}
