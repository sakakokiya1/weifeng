package com.ruoyi.web.controller.common;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.req.UploadVideoRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyun.vod.upload.resp.UploadVideoResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoResponse;
import com.ruoyi.common.constant.PersonConstants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.OSSEntity;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.outpatient.common.MqConstants;
import com.ruoyi.outpatient.core.SecurityUtils;
import com.ruoyi.outpatient.domain.*;
import com.ruoyi.outpatient.mapper.*;
import com.ruoyi.outpatient.service.IBlogService;
import com.ruoyi.outpatient.utils.MySecurityUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.geo.Point;
import org.springframework.data.repository.query.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ruoyi.web.controller.common.Video.initVodClient;
import static com.ruoyi.web.controller.common.aaa.getVideoInfo;

@RestController
@RequestMapping("/upload")
public class UploadOSSController {


    @Autowired
    private OSSEntity ossEntity;

    @Autowired
    private IBlogService blogService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private IBlogMapper blogMapper;

    @Autowired
    private ITagMapper tagMapper;

    @Autowired
    private IMiddleMapper middleMapper;

    @Autowired
    private IFollowMapper followMapper;


    @Autowired
    private IBlogTagMapper blogTagMapper;

    @Autowired
    private SecurityUtils securityUtils;

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @Value("${url.images}")
    private String URLHeader;



    @PostMapping("/image")
    public AjaxResult uploadOSSImages(@NotBlank(message = "图片不能为空") MultipartFile[] files)
    {



        String endpoint = ossEntity.getEndpoint();
        String accessKeyId = ossEntity.getAccessKeyId();
        String accessKeySecret = ossEntity.getAccessKeySecret();
        String bucketName = ossEntity.getBucketName();

//        if (!(MimeTypeUtils.IMAGE_JPG.equals(files[0].getContentType()) || MimeTypeUtils.IMAGE_JPEG.equals(files[0].getContentType()))) {
//
//        }


        StringBuffer buffer = new StringBuffer();


//        1.如果图片不存在或上传的图片大于9,则失败
        if (files.length < 0 || files.length > 9) {
            return AjaxResult.error("上传失败");
        }


//        2.将上传的文件进行遍历
        for (int i = 0; i < files.length; i++) {


//            2.2阿里云只有image/jpg 或 image/jpeg格式才能进行浏览
//            if (!MimeTypeUtils.IMAGE_JPG.equals(files[i].getContentType()) || !MimeTypeUtils.IMAGE_JPEG.equals(files[i].getContentType())) {
//                return AjaxResult.error("ContentType格式不正确");
//            }
//             创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            try {
//                1.将上传的图片转为输入流 用于存入OSSClient实例
                InputStream inputStream1 = files[i].getInputStream();

                String date = DateUtils.getDate();
//                2.设置图片名     yyyy-mm-dd/ +  获取的是图片的完整名称
                String filename = URLHeader+date + "/" + UUID.randomUUID() + files[i].getOriginalFilename();


//                    2.2 将多张图片的url字符串不断放入StringBuffer,用于下面存储到blog对象中
//                    如果图片名存储在^使用#进行替换
                if (filename.contains("^")) {
                    filename.replace('^', '#');
                }
                buffer.append(filename);
                buffer.append("^");

                // 创建PutObject请求。
                ossClient.putObject(bucketName, filename, inputStream1);
            } catch (OSSException oe) {
                System.out.println("Caught an OSSException, which means your request made it to OSS, "
                        + "but was rejected with an error response for some reason.");
                System.out.println("Error Message:" + oe.getErrorMessage());
                System.out.println("Error Code:" + oe.getErrorCode());
                System.out.println("Request ID:" + oe.getRequestId());
                System.out.println("Host ID:" + oe.getHostId());
            } catch (ClientException ce) {
                System.out.println("Caught an ClientException, which means the client encountered "
                        + "a serious internal problem while trying to communicate with OSS, "
                        + "such as not being able to access the network.");
                System.out.println("Error Message:" + ce.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (ossClient != null) {
                    ossClient.shutdown(); //关闭流
                    System.out.println("上传完成");
                }
            }
        }


        return AjaxResult.success("图片上传完成").put("blog_image",buffer.toString());

    }



    @PostMapping("/video")
    public AjaxResult uploadOSSImages(@NotBlank(message = "图片不能为空") MultipartFile video,
                                      @RequestParam("type")String type) throws IOException {


//        子账户
        String accessKeyId = ossEntity.getAccessKeyId();
        String accessKeySecret = ossEntity.getAccessKeySecret();


        String date = DateUtils.getDate();
//                2.设置文件名     yyyy-mm-dd/ +  获取的是图片的完整名称
        String filename = date + "/" + UUID.randomUUID() + video.getOriginalFilename();

        InputStream inputStream = video.getInputStream();
//            UploadVideoRequest request = new UploadVideoRequest(accessKeyId, accessKeySecret,files[0].getOriginalFilename(),filename);
//            /* 可指定分片上传时每个分片的大小，默认为2M字节 */
//            request.setPartSize(2 * 1024 * 1024L);
//            /* 可指定分片上传时的并发线程数，默认为1，(注：该配置会占用服务器CPU资源，需根据服务器情况指定）*/
//            request.setTaskNum(1);

        UploadStreamRequest uploadStreamRequest = new UploadStreamRequest(accessKeyId, accessKeySecret, type, filename, inputStream);
//            UploadVideoImpl uploader = new UploadVideoImpl();
//            UploadVideoResponse response = uploader.uploadVideo(request);
        UploadVideoImpl uploader = new UploadVideoImpl();
        UploadStreamResponse response = uploader.uploadStream(uploadStreamRequest);

        if (response.isSuccess()) {
            System.out.print("VideoId=" + response.getVideoId() + "\n");
//            获取vidoe的id用来获取播放地址
            String videoName=response.getVideoId();



            return AjaxResult.success("上传视频成功").put("videoName",videoName);
        } else {
            /* 如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因 */
            System.out.print("VideoId=" + response.getVideoId() + "\n");
            System.out.print("ErrorCode=" + response.getCode() + "\n");
            System.out.print("ErrorMessage=" + response.getMessage() + "\n");
            return AjaxResult.error("上传失败");
        }

    }




    @PostMapping("uploadBlog")
    public AjaxResult uploadOSS(
            @Validated  Blog blog,
            @RequestParam(value = "flag",required = false)String flag,
            HttpServletRequest servletRequest
    ) throws IOException, com.aliyuncs.exceptions.ClientException {










        if (flag!=null && flag.equals("video")) {
//        将视频ID转为url地址
            DefaultAcsClient client = initVodClient(ossEntity.getAccessKeyId(), ossEntity.getAccessKeySecret());
            GetPlayInfoResponse response = new GetPlayInfoResponse();

            try {

                GetPlayInfoRequest request = new GetPlayInfoRequest();
                request.setVideoId(blog.getBlogImage()); //videoId转url
                response = client.getAcsResponse(request);
//            response =getPlayInfo(client);
                List<GetPlayInfoResponse.PlayInfo> playInfoList = response.getPlayInfoList();
                //播放地址
                for (GetPlayInfoResponse.PlayInfo playInfo : playInfoList) {
                    System.out.print("PlayInfo.PlayURL = " + playInfo.getPlayURL() + "\n");
//                    blog.setBlogImage(playInfo.getPlayURL()); //转url
                    String url=playInfo.getPlayURL();
                    String[] split = url.split("\\?");
                    blog.setFirstFrame(split[0]); //转url
                    blog.setVideoOrImage(PersonConstants.isVideo);//设为视频标志
                    System.out.println("PlayInfo::"+playInfo.toString());

                }
                //Base信息
                System.out.print("VideoBase.Title = " + response.getVideoBase().getTitle() + "\n");
            } catch (Exception e) {
                System.out.print("ErrorMessage = " + e.getLocalizedMessage());
            }
            System.out.print("RequestId = " + response.getRequestId() + "\n");


        }




        if (flag!=null && flag.equals("video")){
            //            获取第一帧地址
//            =======================================================
            DefaultAcsClient client = initVodClient(ossEntity.getAccessKeyId(), ossEntity.getAccessKeySecret());
            GetVideoInfoResponse response = new GetVideoInfoResponse();
            try {
                GetVideoInfoRequest request = new GetVideoInfoRequest();
                request.setVideoId(blog.getBlogImage());
                response = getVideoInfo(client);
                System.out.print("Title = " + response.getVideo().getTitle() + "\n");
                System.out.print("Description = " + response.getVideo().getDescription() + "\n");
                System.out.println("URL"+response.getVideo().getCoverURL());
//                blog.setFirstFrame( response.getVideo().getCoverURL());
                String url=response.getVideo().getCoverURL();
                String[] split = url.split("\\?");
                blog.setBlogImage( split[0]); //转url
            } catch (Exception e) {
                System.out.print("ErrorMessage = " + e.getLocalizedMessage());
            }
            System.out.print("RequestId = " + response.getRequestId() + "\n");

        }







        //        数据库与redis编码板块================================================

//        当全部上传OSS完成后 1.先存数据库 2.存Redis
//        1.分两份存储,在数据库中加索引,person只需要存储redis





//        MySQL模块

        Long loginPersonId = securityUtils.getLoginPersonId();

//        1.先插入blog表获取id，迭代得到tag数组id，再插入中间表
        blog.setPersonId(loginPersonId);
        blog.setBlogImage(blog.getBlogImage()); //图片url 视频在上面插入了
        int b=blogMapper.insertBlog(blog);  //得到blogid

//        2.将blogid放入中间表
        BlogTag blogTag=new BlogTag();
        blogTag.setBlogId(blog.getBlogId());

//        3.遍历得到tag每个标签
        for (int i=0;i<blog.getTag().getTagNameArray().length;i++){


//            3.1 将tag数组拆分放入tagname
            blog.getTag().setTagName(blog.getTag().getTagNameArray()[i]);
//            3.2 插入tag获取id
            tagMapper.insertTag(blog.getTag());
//            3.3 将tagid和blogid存入中间表
            blogTag.setTagId(blog.getTag().getTagId());
            blogTagMapper.insertBlogTagById(blogTag); //插入中间表
        }







//        // TODO: 2022/4/22 优化 性能优化 Redis 是内存级操作，所以单次的查询会很快。
//        //  但是如果我们的实现中会进行多次的 Redis 操作，Redis 的多次连接时间可能是不必要时间消耗。
//        //  通过使用MULTI命令，开启一个事务，将 Redis 的多次操作放在一个事务中，
//        //  最后通过EXEC来进行原子性执行（注意：这里所谓的事务，只是将多个操作在一次连接中执行，
//        //  如果执行过程中遇到失败，是不会回滚的）。


//                                    redis模块 存储tag的首字母并设置浏览量：用于搜索的回显
////       1. 获取前端的tagName数组
        String[] tagName =blog.getTag().getTagNameArray();
        for (String s:tagName) { //将取出单个tagName
//            1.tag存储blogId      2.检索tag总量进行分段存储
            Long blogidSumByTag = redisCache.getCacheZSetZCard(PersonConstants.TAG_BLOGID+s);
//            总数%100，每个子zset最多存储100个 形式如（篮球：0） 200
            if (blogidSumByTag < 100) {
                redisCache.setCacheZSet(PersonConstants.TAG_BLOGID  + 0, blog.getBlogId(), System.currentTimeMillis());
            } else {
                Long i = blogidSumByTag % 100;
//                因为在zset存储的每个blogid都不同，不需要检索直接存储最后一个
                redisCache.setCacheZSet(PersonConstants.TAG_BLOGID + i, blog.getBlogId(), System.currentTimeMillis());
            }
//            2.存储不存在的关键字集合,用于搜索回显 篮：篮球 （时间戳可以改为其他的)
            if (StringUtils.isNotNull(redisCache.getCacheZSet(PersonConstants.TAG_TAGNAMES + ":" + s.substring(0, 1), s))) {
                //                数据量不大不需要子集合
                redisCache.setCacheZSet(PersonConstants.TAG_TAGNAMES + ":" + s.substring(0, 1), s, System.currentTimeMillis());
            }
//            3. tagName的浏览量  进行HyperLogLog的浏览量设置 tagName: {blogid 不重复的id }
            redisCache.setCacheHyperLogLog(s,blog.getBlogId());
        }


////        4.将blog信息存入hash中

//        // TODO: 2022/4/22 将标签存储如redis
//        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"tagString",blog.getTag().getTagNameArray().toString());


//        如果用户有上传位置则还要放到GEO 用来查找
        if (blog.getNearby()!=null) {
//            将用户上传的坐标封装
            Point point = new Point(blog.getNearby().getX(),blog.getNearby().getY());
//            添加到redis地图中
            redisCache.opsForGeoAdd("nerby:city:"+blog.getBlogId(),point,blog.getNearby().getLocation());
        }


        // TODO: 2022/6/12 后期直接删除，双重更新暂用redis内存
//        将所有上传的信息放到redis的hash中
//        Map<String, Object> map = BeanUtil.beanToMap(blog);
//        redisCache.setCacheMap(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),map);

//        记录用户发布的笔记id集合
        redisCache.setCacheZSet("person:blogid:"+loginPersonId,blog.getBlogId(),System.currentTimeMillis());

//dw
        // TODO: 2022/6/12 使用rabbmq
//        使用异步：记录博主更新了blog,用于推送粉丝
        rabbitTemplate.convertAndSend(MqConstants.CHAT_EXCHANGE,MqConstants.INSERT_BLOGTOFANS_ROUTING_KEY,loginPersonId);


        return AjaxResult.success("分享信息上传完成");


    }



//    @PostMapping("/images")
//    public AjaxResult uploadOSS(
//            @NotBlank(message = "图片不能为空")
//            MultipartFile[] files,
////            @RequestBody Tag tag,
//            @Validated  Blog blog,
//            HttpServletRequest servletRequest
//            ) throws IOException {
//
//
//
//
//
////  OSS上传板块==========================================================
//
//        String endpoint = ossEntity.getEndpoint();
//        String accessKeyId = ossEntity.getAccessKeyId();
//        String accessKeySecret = ossEntity.getAccessKeySecret();
//        String bucketName = ossEntity.getBucketName();
//
//
////        System.out.println(!MimeTypeUtils.IMAGE_JPG.equals(files[0].getContentType()));
////        System.out.println(!MimeTypeUtils.IMAGE_JPEG.equals(files[0].getContentType()));
//        if (!(MimeTypeUtils.IMAGE_JPG.equals(files[0].getContentType()) || MimeTypeUtils.IMAGE_JPEG.equals(files[0].getContentType()))) {
//
//        }
//
//
//
//
//        StringBuffer buffer =   new StringBuffer();
//
//        if (servletRequest.getHeader("type").equals("image")) {
//
//
////        1.如果图片不存在或上传的图片大于9,则失败
//            if (files.length < 0 || files.length > 9) {
//                return AjaxResult.error("上传失败");
//            }
//
//
////        2.将上传的文件进行遍历
//            for (int i = 0; i < files.length; i++) {
//
//
////            2.2阿里云只有image/jpg 或 image/jpeg格式才能进行浏览
//                if (!MimeTypeUtils.IMAGE_JPG.equals(files[i].getContentType()) || !MimeTypeUtils.IMAGE_JPEG.equals(files[i].getContentType())) {
//                    return AjaxResult.error("ContentType格式不正确");
//                }
////             创建OSSClient实例。
//                OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
//                try {
////                1.将上传的图片转为输入流 用于存入OSSClient实例
//                    InputStream inputStream1 = files[i].getInputStream();
//
//                    String date = DateUtils.getDate();
////                2.设置文件名     yyyy-mm-dd/ +  获取的是图片的完整名称
//                    String filename = URLHeader+date + "/" + UUID.randomUUID() + files[i].getOriginalFilename();
//
//
////                    2.2 将多张图片的url字符串不断放入StringBuffer,用于下面存储到blog对象中
////                    如果图片名存储在^使用#进行替换
//                    if (filename.contains("^")) {
//                        filename.replace('^', '#');
//                    }
//                    buffer.append(filename);
//                    buffer.append("^");
//
//                    // 创建PutObject请求。
//                    ossClient.putObject(bucketName, filename, inputStream1);
//                } catch (OSSException oe) {
//                    System.out.println("Caught an OSSException, which means your request made it to OSS, "
//                            + "but was rejected with an error response for some reason.");
//                    System.out.println("Error Message:" + oe.getErrorMessage());
//                    System.out.println("Error Code:" + oe.getErrorCode());
//                    System.out.println("Request ID:" + oe.getRequestId());
//                    System.out.println("Host ID:" + oe.getHostId());
//                } catch (ClientException ce) {
//                    System.out.println("Caught an ClientException, which means the client encountered "
//                            + "a serious internal problem while trying to communicate with OSS, "
//                            + "such as not being able to access the network.");
//                    System.out.println("Error Message:" + ce.getMessage());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } finally {
//                    if (ossClient != null) {
//                        ossClient.shutdown(); //关闭流
//                        System.out.println("上传完成");
//                    }
//                }
//            }
//
//        }else if (servletRequest.getHeader("type").equals("video")){
//
//            String date = DateUtils.getDate();
////                2.设置文件名     yyyy-mm-dd/ +  获取的是图片的完整名称
//            String filename = date + "/" + UUID.randomUUID() + files[0].getOriginalFilename();
//
//            InputStream inputStream = files[0].getInputStream();
////            UploadVideoRequest request = new UploadVideoRequest(accessKeyId, accessKeySecret,files[0].getOriginalFilename(),filename);
////            /* 可指定分片上传时每个分片的大小，默认为2M字节 */
////            request.setPartSize(2 * 1024 * 1024L);
////            /* 可指定分片上传时的并发线程数，默认为1，(注：该配置会占用服务器CPU资源，需根据服务器情况指定）*/
////            request.setTaskNum(1);
//
//            UploadStreamRequest uploadStreamRequest = new UploadStreamRequest(accessKeyId, accessKeySecret, blog.getBlogTheme(), filename, inputStream);
////            UploadVideoImpl uploader = new UploadVideoImpl();
////            UploadVideoResponse response = uploader.uploadVideo(request);
//            UploadVideoImpl uploader = new UploadVideoImpl();
//            UploadStreamResponse response = uploader.uploadStream(uploadStreamRequest);
//
//            if (response.isSuccess()) {
//                System.out.print("VideoId=" + response.getVideoId() + "\n");
//                buffer.append(response.getVideoId().split("=")[1]);
//            } else {
//                /* 如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因 */
//                System.out.print("VideoId=" + response.getVideoId() + "\n");
//                System.out.print("ErrorCode=" + response.getCode() + "\n");
//                System.out.print("ErrorMessage=" + response.getMessage() + "\n");
//            }
//
//        }else {
//            return AjaxResult.error("请上传视频或图片");
//        }
//
//
//
//
//
//
//
//
//
//
////        数据库与redis编码板块================================================
//
////        当全部上传OSS完成后 1.先存数据库 2.存Redis
////        1.分两份存储,在数据库中加索引,person只需要存储redis
//
//
//
//
////        MySQL模块
//        //  2022/4/23 认证后再取消注释
//        Long loginPersonId = securityUtils.getLoginPersonId();
//
////        1.先插入blog获取id，迭代得到tag数组id，再插入中间表
//        blog.setPersonId(loginPersonId);
//        blog.setBlogImage(buffer.toString());
//        int b=blogMapper.insertBlog(blog);  //得到blogid
//
////        2.将blogid放入中间表
//        BlogTag blogTag=new BlogTag();
//        blogTag.setBlogId(blog.getBlogId());
//
////        3.遍历得到tag每个标签
//        for (int i=0;i<blog.getTag().getTagNameArray().length;i++){
//
//
////            3.1 将tag数组拆分放入tagname
//            blog.getTag().setTagName(blog.getTag().getTagNameArray()[i]);
////            3.2 插入tag获取id
//            tagMapper.insertTag(blog.getTag());
////            3.3 将tagid和blogid存入中间表
//            blogTag.setTagId(blog.getTag().getTagId());
//            blogTagMapper.insertBlogTagById(blogTag); //插入中间表
//        }
//
//
////     todo   分享文章存储完后,推送给关注自己的那些粉丝
////        1.查询作者的所有粉丝
////        2.推送到所有的粉丝中
////            2.1 每个粉丝都用zset存储，add（personod，followid）
//
//
//
//
//
////        // TODO: 2022/4/22 优化 性能优化 Redis 是内存级操作，所以单次的查询会很快。
////        //  但是如果我们的实现中会进行多次的 Redis 操作，Redis 的多次连接时间可能是不必要时间消耗。
////        //  通过使用MULTI命令，开启一个事务，将 Redis 的多次操作放在一个事务中，
////        //  最后通过EXEC来进行原子性执行（注意：这里所谓的事务，只是将多个操作在一次连接中执行，
////        //  如果执行过程中遇到失败，是不会回滚的）。
//
//
////        redis模块存储
//
////        // TODO: 2022/4/22 优化 看是用set还是zset
//        // TODO: 2022/4/22 后期看是否可以加互斥锁
//        // TODO: 2022/4/22 上面的redis更新前提还要进行sql的查找，缓存一致性
//////       1. 获取前端的tagName数组
//        String[] tagName =blog.getTag().getTagNameArray();
//
//        for (String s:tagName) { //将取出单个tagName
////            1.tag存储blogId      2.检索tag总量进行分段存储
//            Long blogidSumByTag = redisCache.getCacheZSetZCard(PersonConstants.TAG_BLOGID+s);
////            总数%100，每个子zset最多存储100个 形式如（篮球：0） 200
//            if (blogidSumByTag < 100) {
//                redisCache.setCacheZSet(PersonConstants.TAG_BLOGID  + 0, blog.getBlogId(), System.currentTimeMillis());
//            } else {
//                Long i = blogidSumByTag % 100;
////                因为在zset存储的每个blogid都不同，不需要检索直接存储最后一个
//                redisCache.setCacheZSet(PersonConstants.TAG_BLOGID + i, blog.getBlogId(), System.currentTimeMillis());
//            }
//
//
////            2.存储不存在的关键字集合,用于搜索回显 篮：篮球 （时间戳可以改为其他的)
//            if (StringUtils.isNotNull(redisCache.getCacheZSet(PersonConstants.TAG_TAGNAMES + ":" + s.substring(0, 1), s))) {
//                //                数据量不大不需要子集合
//                redisCache.setCacheZSet(PersonConstants.TAG_TAGNAMES + ":" + s.substring(0, 1), s, System.currentTimeMillis());
//            }
//
////            3.进行HyperLogLog的浏览量设置 tagName: {blogid 不重复的id }
//            redisCache.setCacheHyperLogLog(s,blog.getBlogId());
//        }
//
//
//////        4.将blog信息存入hash中
////        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"personId",person.getPersonId());
////        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"personName",person.getPersonName());
////        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"avatar",person.getAvatar());
////        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"blogId",blog.getBlogId());
////        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"blogImage",blog.getBlogImage());
////        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"blogTheme",blog.getBlogTheme());
////        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"blogTalk",blog.getBlogTalk());
////        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"createTime",blog.getCreateTime());
////        // TODO: 2022/4/22 将标签存储如redis
////        redisCache.setCacheMapValue(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),"tagString",blog.getTag().getTagNameArray().toString());
//
//
////        如果用户有上传位置则还要放到GEO 用来查找
//        // TODO: 2022/5/19 这里要改
//
//        if (blog.getNearby()!=null) {
//            Point point = new Point(blog.getNearby().getX(),blog.getNearby().getY());
//            redisCache.opsForGeoAdd("nerby"+blog.getBlogId(),point,blog.getNearby().getLocation());
//        }
//
//
////        将所有上传的信息放到redis的hash中
//        Map<String, Object> map = BeanUtil.beanToMap(blog);
//        redisCache.setCacheMap(PersonConstants.BLOG_UPLOAD+blog.getBlogId(),map);
//
//
//        return AjaxResult.success("分享上传完成");
//    }
}
