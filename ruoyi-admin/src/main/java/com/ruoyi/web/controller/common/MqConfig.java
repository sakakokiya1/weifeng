package com.ruoyi.web.controller.common;

import com.ruoyi.outpatient.common.MqConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 伟峰
 * @date 2022/5/14
 * @description: rabbitmq配置类 用于交换机和队列的搬定
 */
@Configuration
public class MqConfig {


//    使用交换机
    @Bean
    public TopicExchange topicExchange()
    {
        return new TopicExchange(MqConstants.CHAT_EXCHANGE,true,false);
    }
//    使用insert队列
    @Bean
    public Queue insertQueue()
    {
        return new Queue(MqConstants.INSERT_QUEUE,true);
    }

//    使用delete队列
    @Bean
    public Queue deleteQueue()
    {
        return new Queue(MqConstants.DELETE_QUEUE,true);
    }

    //    监听上传blog推送到粉丝
    @Bean
    public Queue insertBlogtoFansQueue()
    {
        return new Queue(MqConstants.INSERT_BLOGTOFANS_QUEUE,true);
    }


//    用routingKey来搬定交换机和队列
    @Bean
    public Binding insertBinding()
    {      //将insert队列搬定到topicExchange交换机用INSERT_ROUTING_KEY
        return BindingBuilder.bind(insertQueue()).to(topicExchange())
                .with(MqConstants.INSERT_ROUTING_KEY);
    }

    @Bean
    public Binding deleteBinding()
    {      //将insert队列搬定到topicExchange交换机用INSERT_ROUTING_KEY
        return BindingBuilder.bind(deleteQueue()).to(topicExchange())
                .with(MqConstants.DELETE_ROUTING_KEY);
    }


//    监听上传blog推送到粉丝
    @Bean
    public Binding insertBlogtoFansBinding()
    {      //将insert队列搬定到topicExchange交换机用INSERT_ROUTING_KEY
        return BindingBuilder.bind(insertBlogtoFansQueue()).to(topicExchange())
                .with(MqConstants.INSERT_BLOGTOFANS_ROUTING_KEY);
    }


}
