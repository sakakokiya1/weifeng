package com.ruoyi.web.controller.outpatient;

import cn.hutool.Hutool;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.outpatient.core.SecurityUtils;
import com.ruoyi.outpatient.service.IPersonLoginService;
import com.ruoyi.outpatient.service.IPersonService;
import com.ruoyi.outpatient.service.impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * @author 伟峰
 * @date 2022/4/22
 * @description: 搜索controller
 */

@RestController
@RequestMapping("/search")
public class SearchController extends BaseController {


    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SecurityUtils securityUtils;


    @Autowired
    private IPersonService personService;

    /**
     *  综合排序的标签搜索
     * @param keyword 标签或搜索单词
     * @return 以投喂形式的列表(无限滚动)
     */
    @GetMapping("/keyword")
    public AjaxResult keyWordSearch(
            @NotBlank(message = "搜索关键字不能为空")
            @RequestParam("keyword")String keyword){


//        获得时间戳
        long timeScore = redisCache.getCacheZSetScore(keyword.substring(0, 1), keyword).longValue();

        return null;
    }


    /**
     * 进入搜索页时 （历史记录 热门事件）
     * @return
     */
    @GetMapping("/window")
    public AjaxResult searchPage(){


        Long loginPersonId = securityUtils.getLoginPersonId();

//        搜索历史记录
        //获取10个
        Set<ZSetOperations.TypedTuple> set = redisCache.reverseRangeZSetWithScores("history:tag" + loginPersonId, 0, 10);

        List<String> list=new ArrayList<>();
        for (ZSetOperations.TypedTuple f:set) {
            list.add(f.getValue().toString());
        }


//        热门事件
//        在数据库中查找 浏览器前10的主题 返回
        //            4.1 用户对应全部热门标签
        Long tagSumsx = redisCache.getCacheZSetZCard("recommend:tagscore:" + loginPersonId);
//            4.2取出前50%的标签  用户1:{标签1，标签2，标签3}
        Set<ZSetOperations.TypedTuple> hotTag = redisCache
                .reverseRangeZSetWithScores("recommend:tagscore:" + loginPersonId, 0, tagSumsx.intValue() > 10 ? 10 : tagSumsx.intValue()+1);


//            4.3将取出来的标签转为hashmap{"标签名",分数}

        List<String> arrlist=new ArrayList<>();
        if (hotTag!=null){
            for (ZSetOperations.TypedTuple h:hotTag) {
                arrlist.add(h.getValue().toString());
            }
        }


        HashMap<String,List<String>> map=new HashMap<>(2);
        map.put("history",list);
        map.put("guess",arrlist);
        return AjaxResult.success("搜索展示").put("map",map);

    }


    /**
     *  搜索用户
     * @return
     */
    @GetMapping("/personSearch")
    public AjaxResult personSearch(
            @NotNull(message = "搜索不能为空")
            @RequestParam("tagName")String tagName
    ){
        AjaxResult personList= personService.selectPersonListByTagName(tagName);

        return AjaxResult.success().put("personList",personList);

    }


}
