package com.ruoyi.web.controller.system;

import com.alibaba.druid.sql.ast.SQLDataType;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.system.domain.SysEiSet;
import com.ruoyi.system.mapper.SysEiSetMapper;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysEiSetService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 检查项目配置
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/eiset")
public class SysEiSetController extends BaseController
{
    @Autowired
    private ISysEiSetService eiSetService;


    @Autowired
    private SysEiSetMapper eiSetMapper;

    private ISysConfigService configService;



    /**
     * 获取检查项目配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:eiset:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysEiSet sysEiSet)
    {
        startPage();
        List<SysEiSet> list = eiSetService.selectEiSetList(sysEiSet);
        return getDataTable(list);
    }


    /**
     * 添加检查项目设置
     * @param sysEiSet
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:eiset:add')")
    @PostMapping("/add")
    public AjaxResult eisetAdd(@Validated @RequestBody SysEiSet sysEiSet)
    {
        if (StringUtils.isNotNull(sysEiSet)){
            int row=eiSetService.updateEiSetAdd(sysEiSet);

            return AjaxResult.success("添加成功",row);
        }
        return AjaxResult.error("该检查项目已存在");
    }

    /**
     * 通过ID删除检查项目
     * @param eisetId
     * @return
     */

    @Delete("/{eisetId}")
    @PreAuthorize("@ss.hasPermi('system:eiset:delete')")
    public AjaxResult delete(@PathVariable Long eisetId)
    {
        if (StringUtils.isNotNull(eisetId) && StringUtils.isNotNull(eiSetMapper.selectEiSetById(eisetId)))
        {
            int rows=eiSetService.deleteEiSetById(eisetId);
            return toAjax(rows);
        }
        return toAjax(false);
    }


    /**
     * 修改检查项目设置
     */
    @PreAuthorize("@ss.hasPermi('system:eiset:edit')")
    @PutMapping
    public AjaxResult editEiSet(@Validated @RequestBody SysEiSet sysEiSet)
    {
        if (StringUtils.isNotNull(sysEiSet)){
            int row=eiSetService.editEiSet(sysEiSet);
            return toAjax(row);
        }
        return toAjax(false);


    }








}
