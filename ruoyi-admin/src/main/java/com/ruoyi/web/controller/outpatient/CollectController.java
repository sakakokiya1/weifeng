package com.ruoyi.web.controller.outpatient;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.outpatient.mapper.ICollectMapper;
import com.ruoyi.outpatient.service.ICollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * @author 伟峰
 * @date 2022/6/10
 * @description: 收藏实体类
 */

@RestController
@RequestMapping("/collect")
public class CollectController extends BaseController {



    @Autowired
    private ICollectService collectService;

    /**
     * 对blog进行收藏
     * @param blogId
     * @return
     */
    @GetMapping("/likes")
    public AjaxResult likes(
            @NotNull(message = "blogId不能为空")
            @RequestParam("blogId")Long blogId
    ){

        return  collectService.collectBlog(blogId);

    }



}
