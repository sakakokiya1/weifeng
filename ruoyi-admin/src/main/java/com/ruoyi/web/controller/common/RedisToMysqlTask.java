package com.ruoyi.web.controller.common;

import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.outpatient.mapper.IBlogMapper;
import com.ruoyi.outpatient.mapper.ICommentMapper;
import com.ruoyi.outpatient.mapper.IReplyMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author 伟峰
 * @date 2022/5/17
 * @description: 将redis的数据刷新到mysql 定时任务
 */
public class RedisToMysqlTask {

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private IBlogMapper blogMapper;


    @Autowired
    private ICommentMapper commentMapper;

    @Autowired
    private IReplyMapper replyMapper;

    // 将blog点赞数量刷新到数据库
    public void blogLikeToSql()
    {
//        查看是否过期,如果该key差不多要过期进将他刷新到数据库

        List<Long> longList=blogMapper.selectBlogId();

        HashMap<Long,Long> map=new HashMap<>();


        for (Long lo:longList) {
            Long ttl = redisCache.ttl("blog:likesPlusOne:" + lo, TimeUnit.HOURS);
            System.out.println("时间："+ttl);
//            只要过期时间<3小时就放到新list然后更新到数据库中
            if (ttl>0 && ttl<3){
                Integer cacheObject = (Integer)redisCache.getCacheObject("blog:likesPlusOne:" + lo);
                map.put(lo,Long.valueOf(cacheObject));
//                然后将键删除
                redisCache.deleteObject("blog:likesPlusOne:" + lo);

            }
        }



//        批量更新
        blogMapper.updateBlogLikes(map);


    }



    // 将blog收藏数量刷新到数据库
    public void blogCollectToSql()
    {
//        查看是否过期,如果该key差不多要过期进将他刷新到数据库

        List<Long> longList=blogMapper.selectBlogId();

        HashMap<Long,Long> map=new HashMap<>();


        for (Long lo:longList) {
            Long ttl = redisCache.ttl("blog:collectSum:" + lo, TimeUnit.HOURS);
            System.out.println("时间："+ttl);
//            只要过期时间<3小时就放到新list然后更新到数据库中
            if (ttl>0 && ttl<3){
                Integer cacheObject = (Integer)redisCache.getCacheObject("blog:collectSum:" + lo);
                map.put(lo,Long.valueOf(cacheObject));
//                然后将键删除
                redisCache.deleteObject("blog:collectSum:" + lo);

            }
        }



//        批量更新
        blogMapper.updateBlogLikes(map);
        blogMapper.updateBlogCollects(map);


    }





    // 将评论点赞数量刷新到数据库
    public void commentListToSql(String s, Boolean b, Double d)
    {
//        查看是否过期,如果该key差不多要过期进将他刷新到数据库

        List<Long> longList=commentMapper.selectCommentId();



        ArrayList<Long> pastKey=new ArrayList<>();
        int sum=0;
        for (Long lo:longList) {
            Long ttl = redisCache.ttl("comment:likesPlusOne:" + lo, TimeUnit.HOURS);
//            只要过期时间<3小时就放到新list然后更新到数据库中
            if (ttl>0 && ttl<3){
                pastKey.add(sum,lo);
                sum+=1;
            }
        }


        //        获取点赞总数
        Object cacheObject = redisCache.getCacheObject("comment:likesPlusOne:" + 26);
        Long aLong = Long.valueOf(cacheObject.toString());
        System.out.println("redis总数："+aLong);

    }




    // 将回复点赞数量刷新到数据库
    public void replyListToSql(String s, Boolean b, Double d)
    {
//        查看是否过期,如果该key差不多要过期进将他刷新到数据库

        List<Long> longList=replyMapper.selectReplyId();



        ArrayList<Long> pastKey=new ArrayList<>();
        int sum=0;
        for (Long lo:longList) {
            Long ttl = redisCache.ttl("reply:likesPlusOne:" + lo, TimeUnit.HOURS);
//            只要过期时间<3小时就放到新list然后更新到数据库中
            if (ttl>0 && ttl<3){
                pastKey.add(sum,lo);
                sum+=1;
            }
        }


        //        获取点赞总数
        Object cacheObject = redisCache.getCacheObject("reply:likesPlusOne:" + 26);
        Long aLong = Long.valueOf(cacheObject.toString());
        System.out.println("redis总数："+aLong);

    }

}
