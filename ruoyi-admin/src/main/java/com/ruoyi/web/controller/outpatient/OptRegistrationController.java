package com.ruoyi.web.controller.outpatient;






import com.ruoyi.common.core.controller.*;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.KsDept;
import com.ruoyi.system.mapper.SysKsDeptMapper;
import com.ruoyi.outpatient.mapper.IOptRegistrationMapper;
import com.ruoyi.outpatient.service.IOptRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 门诊挂号
 */
@RestController
@RequestMapping("/outpatient/registration")
public class OptRegistrationController extends BaseController{

    @Autowired
    private static IOptRegistrationService registrationService;


    @Autowired
    private IOptRegistrationMapper registrationMapper;

    @Autowired
    private SysKsDeptMapper ksDeptMapper;


    /**
     *  患者查询科室列表
     * @param
     * @return
     */
    @PreAuthorize("@ss.hasPermi('opt:patient:list')")
    @GetMapping("ksdept")
    public AjaxResult list(KsDept ksDept)
    {

//        List<KsDept> list=ksDeptMapper.selectList(ksDept);


//        查询所有科室列表,且分类好了
        AjaxResult list=registrationService.selectKsDeptList(ksDept);
        return AjaxResult.success(list);



    }

}

