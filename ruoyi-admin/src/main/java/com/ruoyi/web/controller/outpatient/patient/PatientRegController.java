package com.ruoyi.web.controller.outpatient.patient;


import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.outpatient.domain.PatientIdentity;
import com.ruoyi.outpatient.mapper.patient.PatientRegMapper;
import com.ruoyi.outpatient.service.patient.IPatientRegService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * 患者注册管理
 */
@RestController
@RequestMapping("/outpatient/patient")
public class PatientRegController extends BaseController {
    @Autowired
    private IPatientRegService patientRegService;

    @Autowired
    private PatientRegMapper patientRegMapper;


    /**
     * 患者身份证绑定
     * @param identityCard 身份证号
     * @param patientName 患者名字
     * @return int
     */
    @PostMapping("/identity")
    public AjaxResult firstVisit(@Validated @RequestParam("identityCard")String identityCard,
                                 @RequestParam("patientName")String patientName)
    {

        PatientIdentity identity=patientRegService.insertPatientIdentity(identityCard,patientName);
        return AjaxResult.success("认证成功请进行登录").put("patientIdentity",identity);

    }


//    诊疗卡登录 认证
    @PostMapping("/login")
    public AjaxResult patientLogin(@Validated @RequestParam("medicalCard")String medicalCard,
                                   @RequestParam("medicalPassword")String medicalPassword,
                                   HttpServletRequest request)
    {
        StringBuffer buffer=new StringBuffer();

        buffer.append(request.getHeader("patient"));
        buffer.append("_");
        buffer.append(medicalCard);


       String token= patientRegService.patientLogin(buffer.toString(),medicalPassword);

       if (StringUtils.isNotNull(token))
       {
           return AjaxResult.success("登录成功").put("token",token);
       }
       return AjaxResult.error("请先认证");


    }


}
