package com.ruoyi.web.controller.common;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoResponse;

import java.util.List;

import static com.ruoyi.web.controller.common.Video.initVodClient;

/**
 * @author 伟峰
 * @date 2022/5/14
 * @description:
 */
public class GetVideoTest {

    /*获取播放地址函数*/
    public static GetPlayInfoResponse getPlayInfo(DefaultAcsClient client) throws Exception {
        GetPlayInfoRequest request = new GetPlayInfoRequest();
        request.setVideoId("fc43987f60e345bc866fa6348501b13d,441b095ce825421884438ffe33373d29");


        return client.getAcsResponse(request);
    }

    public static void main(String[] args) throws ClientException {
        DefaultAcsClient client = initVodClient("LTAI5t6gigdF6DhkXxTA6bb7", "unUJcpvIuTjkvINLlgdBdToZp6YfN2");
        GetPlayInfoResponse response = new GetPlayInfoResponse();
        try {
            response = getPlayInfo(client);
            List<GetPlayInfoResponse.PlayInfo> playInfoList = response.getPlayInfoList();
            //播放地址
            for (GetPlayInfoResponse.PlayInfo playInfo : playInfoList) {
                System.out.print("PlayInfo.PlayURL = " + playInfo.getPlayURL() + "\n");
            }
            //Base信息
            System.out.print("VideoBase.Title = " + response.getVideoBase().getTitle() + "\n");
        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
        }
        System.out.print("RequestId = " + response.getRequestId() + "\n");
    }



}
